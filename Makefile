#!/usr/bin/make

SHELL = /bin/bash

.PHONY : help up down shell
.DEFAULT_GOAL : help

# This will output the help for each task. thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Show this help
	@printf "\033[33m%s:\033[0m\n" 'Available commands'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "  \033[32m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

up: ## Create and start container
	docker-compose up -d --remove-orphans shopware

down: ## Stop container
	docker-compose down

shell: ## Start shell into container
	docker-compose exec -it shopware bash
