<?php declare(strict_types=1);

namespace SwExtendPackstation6\Controller;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use \Tonur\Packstation\Storefront\Controller\PackstationController as BasePackstationController;

/**
 * @Route(defaults={"_routeScope"={"store-api"}})
 */
class PackstationController extends BasePackstationController
{
    /**
     * @Route("/store-api/packstation/search", name="store-api.packstation.search", methods={"POST"})
     * @param Request $request
     * @param SalesChannelContext $context
     * @return JsonResponse
     */
    public function searchPackstation(Request $request, SalesChannelContext $context): JsonResponse
    {
        return parent::searchPackstation($request, $context);
    }
}