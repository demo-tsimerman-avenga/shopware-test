<?php declare(strict_types=1);

namespace SwExtendPackstation6;

use Shopware\Core\Framework\Plugin;

class SwExtendPackstation6 extends Plugin
{
    public const BUNDLE_NAME = 'SwExtendPackstation6';
}