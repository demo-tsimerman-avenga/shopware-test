<?php declare(strict_types=1);

namespace SwExtendPackstation6\Subscriber;

use Psr\Log\LoggerAwareTrait;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\System\Country\CountryCollection;
use Shopware\Core\System\SalesChannel\Event\SalesChannelContextCreatedEvent;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tonur\Packstation\TonurPackstation6;

class PackstationSubscriber implements EventSubscriberInterface
{
    /**
     * @var SystemConfigService
     */
    private SystemConfigService $systemConfigService;

    /**
     * @var EntityRepositoryInterface
     */
    private EntityRepositoryInterface $countryRepository;

    use LoggerAwareTrait;

    /**
     * @param SystemConfigService $systemConfigService
     * @param EntityRepositoryInterface $countryRepository
     */
    public function __construct(
        SystemConfigService       $systemConfigService,
        EntityRepositoryInterface $countryRepository
    )
    {
        $this->systemConfigService = $systemConfigService;
        $this->countryRepository = $countryRepository;
    }

    public static function getSubscribedEvents(): array {
        return [
            SalesChannelContextCreatedEvent::class => 'onSalesChannelContextCreated'
        ];
    }

    public function onSalesChannelContextCreated(SalesChannelContextCreatedEvent $event)
    {
        if (!$this->isPluginActive(TonurPackstation6::BUNDLE_NAME, $event->getSalesChannelContext()->getSalesChannel()->getId())) {
            return;
        }

        $config = $this->systemConfigService->get(TonurPackstation6::BUNDLE_NAME . '.config');
        $countryUuidGermany = $this->getCountryUuidByIso($event->getContext());

        $extensions = [
            'packstation' => new ArrayStruct([
                'config' => $config,
                'countryUuidGermany' => $countryUuidGermany,
            ])
        ];

        $event->getContext()->setExtensions($extensions);
    }

    /**
     * @param string $name
     * @param string|null $salesChannelId
     * @return bool|float|int|array|string|null
     */
    private function isPluginActive(string $name, ?string $salesChannelId = null)
    {
        return $this->systemConfigService->get($name . '.config.pluginActive', $salesChannelId);
    }

    /**
     * @param $context
     * @param string $iso
     * @return null
     */
    private function getCountryUuidByIso($context, string $iso = 'DE')
    {
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('country.active', true))
            ->addFilter(new EqualsFilter('country.iso', $iso));

        /** @var CountryCollection $countries */
        $countries = $this->countryRepository->search($criteria, $context)->getEntities();

        $country = $countries->first();

        return $country ? $country->getId() : null;
    }
}
