<?php declare(strict_types=1);

namespace Tonur\Packstation\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1567170938Packstation extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1567170938;
    }

    public function update(Connection $connection): void
    {
        $connection->executeStatement('
            CREATE TABLE IF NOT EXISTS `repertus_packstation` (
              `id` BINARY(16) NOT NULL,
              `customer_address_id` BINARY(16) NOT NULL,
              `post_number` VARCHAR(255) NOT NULL,
              `packstation_number` VARCHAR(255) NOT NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `customer_address_id` (`customer_address_id`),
              CONSTRAINT `fk.repertus_packstation.customer_address_id` FOREIGN KEY (`customer_address_id`)
                REFERENCES  `customer_address` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
    }

    public function updateDestructive(Connection $connection): void
    {
    }
}
