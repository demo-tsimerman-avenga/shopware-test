<?php declare(strict_types=1);

namespace Tonur\Packstation\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1576750654AddPackstationType extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1576750654;
    }

    public function update(Connection $connection): void
    {
        $connection->executeStatement('alter table repertus_packstation add packstation_type varchar(255) null');
    }

    public function updateDestructive(Connection $connection): void
    {
    }
}
