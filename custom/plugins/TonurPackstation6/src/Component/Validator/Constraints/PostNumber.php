<?php

namespace Tonur\Packstation\Component\Validator\Constraints;

use Symfony\Component\Validator\Constraints\Regex;

class PostNumber extends Regex
{
    public const POST_NUMBER_ERROR = '75dc5ded-0452-4164-8032-2fe0519e57ad';

    protected static $errorNames = [
        self::POST_NUMBER_ERROR => 'POST_NUMBER_ERROR',
    ];

}
