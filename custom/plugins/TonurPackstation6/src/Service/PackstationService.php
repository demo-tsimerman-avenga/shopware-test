<?php declare(strict_types=1);

namespace Tonur\Packstation\Service;

use Psr\Log\LoggerAwareTrait;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\InconsistentCriteriaIdsException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tonur\Packstation\Checkout\Packstation\PackstationEntity;
use Tonur\Packstation\Lib\DHLStandortsuche\DHLStandortsucheClient;
use Tonur\Packstation\TonurPackstation6;

class PackstationService
{
    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    /**
     * @var EntityRepositoryInterface
     */
    private $repertusPackstationRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $customerAddressRepository;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    use LoggerAwareTrait;

    /**
     * PackstationService constructor.
     * @param SystemConfigService $systemConfigService
     * @param EntityRepositoryInterface $repertusPackstationRepository
     * @param EntityRepositoryInterface $customerAddressRepository
     * @param TranslatorInterface $translator
     */
    public function __construct(
        SystemConfigService $systemConfigService,
        EntityRepositoryInterface $repertusPackstationRepository,
        EntityRepositoryInterface $customerAddressRepository,
        TranslatorInterface $translator
    )
    {
        $this->systemConfigService = $systemConfigService;
        $this->repertusPackstationRepository = $repertusPackstationRepository;
        $this->customerAddressRepository = $customerAddressRepository;
        $this->translator = $translator;
    }

    /**
     * @param array $address
     * @param $context
     * @return array|bool
     * @throws InconsistentCriteriaIdsException
     */
    public function getPackstationInfo($address, $context)
    {
        $postNumberField = $this->systemConfigService->get(TonurPackstation6::BUNDLE_NAME . '.config.savePostnumberField');

        if (!empty($_POST) && array_key_exists('repertusPackstationPostNumber', $_POST)) {
            $postNumber = $_POST['repertusPackstationPostNumber'];
        }
        else {
            $postNumber = $address[$postNumberField] ?? null;
        }

        $packstationType = $this->getPackstationTypeByAddress($address);
        $packstationNumber = $this->getPackstationNumberFromAddress($address, $packstationType);

        if ($packstationType !== '' && $packstationNumber !== '' && $postNumber !== null) {
            $packstationInfo = [
                'customerAddressId' => $address['id'],
                'postNumber' => $postNumber,
                'packstationType' => $packstationType,
                'packstationNumber' => $packstationNumber
            ];

            $packstationInfo['updateCustomerAddress']['id'] = $address['id'];
            $packstationInfo['updateCustomerAddress'][$postNumberField] = $postNumber;

            /** @var EntitySearchResult $packstation */
            $packstation = $this->getPackstationForCustomerAddressId($address['id'], $context);

            /** @var PackstationEntity $packstationElement */
            foreach ($packstation->getElements() as $packstationElement) {
                $packstationInfo['id'] = $packstationElement->getUniqueIdentifier();
            }

            return $packstationInfo;
        }

        return false;
    }

    /**
     * @param $customerAddressId
     * @param $context
     * @return EntitySearchResult
     * @throws InconsistentCriteriaIdsException
     */
    public function getPackstationForCustomerAddressId($customerAddressId, $context): EntitySearchResult
    {
        return $this->repertusPackstationRepository->search(
            (new Criteria())->addFilter(new EqualsFilter('customerAddressId', $customerAddressId)),
            $context
        );
    }

    /**
     * @param array $packstationInfo
     * @param Context $context
     */
    public function savePackstation($packstationInfo, $context): void
    {
        $this->repertusPackstationRepository->upsert([$packstationInfo], $context);
        if (array_key_exists('updateCustomerAddress', $packstationInfo)) {
            $this->customerAddressRepository->upsert([$packstationInfo['updateCustomerAddress']], $context);
        }
    }

    /**
     * @param string $street
     * @param string $streetNo
     * @param string $zip
     * @param string $city
     * @return array
     */
    public function searchPackstation($street, $streetNo, $zip, $city): array
    {
        try {
            $standortsuche = new DHLStandortsucheClient();
            return $standortsuche->determinePackstationsFilialeDirektByAddress($street, $streetNo, $zip, $city) ?: [];
        }
        catch (\Exception $exception) {
            return [$exception->getMessage()];
        }
    }

    public function getPackstationTypeByAddress($address): string
    {
        $street = $address['street'] ?? '';

        if (strpos($street, $this->translator->trans('tonur.packstation.street_values.packstation')) === 0) {
            return 'P';
        }

        if (strpos($street, $this->translator->trans('tonur.packstation.street_values.post_office')) === 0) {
            return 'F';
        }

        return '';
    }

    public function getPackstationNumberFromAddress($address, $packstationType): string
    {
        $street = $address['street'] ?? '';

        $typeString = '';

        switch ($packstationType) {
            case 'F':
                $typeString = $this->translator->trans('tonur.packstation.street_values.post_office');
                break;
            case 'P':
                $typeString = $this->translator->trans('tonur.packstation.street_values.packstation');
                break;
        }

        return trim(substr($street, strlen($typeString)));
    }

    public function deletePackstationByAddressId($addressId, Context $context)
    {
        $ids = $this->getPackstationForCustomerAddressId($addressId, $context)->getIds();

        if ($ids) {
            $ids = array_values($ids);
            $ids = array_map(function ($id) {return ['id' => $id];}, $ids);
            $this->logger->debug(__METHOD__. ': Delete packstations by addressIds', ['ids' =>  $ids]);
            $this->repertusPackstationRepository->delete($ids, $context);
        }
    }

}
