<?php


namespace Tonur\Packstation\Core\Checkout\Cart\Address\Error;


use Shopware\Core\Checkout\Cart\Error\Error;

class ProductShippingAddressBlockedError extends Error
{
    private const KEY = 'tonur.packstation.product-shipping-address-blocked';

    /**
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;

        $this->message = sprintf(
            'Mindestens ein Produkt kann nicht an eine Packstation versendet werden. Bitte wählen Sie eine andere Lieferadresse.'
        );

        parent::__construct($this->message);
    }

    public function getLevel(): int
    {
        return self::LEVEL_ERROR;
    }

    public function blockOrder(): bool
    {
        return true;
    }

    public function getMessageKey(): string
    {
        return self::KEY;
    }

    public function getId(): string
    {
        return $this->getKey();
    }

    public function getKey(): string
    {
        return sprintf('%s-%s', self::KEY, $this->name);
    }

    public function getParameters(): array
    {
        return ['name' => $this->name];
    }
}
