<?php

namespace Tonur\Packstation\Core\Checkout\Cart\Address;

use Psr\Log\LoggerAwareTrait;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartValidatorInterface;
use Shopware\Core\Checkout\Cart\Error\Error;
use Shopware\Core\Checkout\Cart\Error\ErrorCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressEntity;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Framework\Context;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Tonur\Packstation\Core\Checkout\Cart\Address\Error\ProductShippingAddressBlockedError;
use Tonur\Packstation\Service\PackstationService;
use Tonur\Packstation\TonurPackstation6;
use Tonur\Packstation\Util\PluginConfigUtil;

class AddressValidator implements CartValidatorInterface
{
    /** @var PackstationService */
    private $packstationService;

    /** @var PluginConfigUtil */
    private $pluginConfigUtil;

    use LoggerAwareTrait;

    public function __construct(
        PackstationService $packstationService,
        PluginConfigUtil $pluginConfigUtil
    ) {
        $this->packstationService = $packstationService;
        $this->pluginConfigUtil = $pluginConfigUtil;
    }

    public function validate(Cart $cart, ErrorCollection $errorCollection, SalesChannelContext $salesChannelContext): void
    {
        if (!$this->isPluginActive($salesChannelContext)) {
            return;
        }

        if (!$this->isCartValidatorConfigEnabled($salesChannelContext)) {
            return;
        }

        $this->removeErrors($cart, $errorCollection);

        $customer = $salesChannelContext->getCustomer();
        if ($customer === null) {
            return;
        }

        if ($cart->getLineItems()->count() === 0) {
            return;
        }

        $this->validateShippingAddress($cart, $customer->getActiveShippingAddress(), $errorCollection, $salesChannelContext);
    }

    private function validateShippingAddress(
        Cart $cart,
        ?CustomerAddressEntity $shippingAddress,
        ErrorCollection $errors,
        SalesChannelContext $salesChannelContext
    ): void {

        if ($shippingAddress !== null) {
            if ($this->isPackstationAddress($shippingAddress, $salesChannelContext->getContext())
                && !$this->collectionCanBeSentToPackstation($cart->getLineItems())) {

                $errors->add($this->getProductShippingAddressBlockedError());

            }
        }
    }

    private function isPackstationAddress(CustomerAddressEntity $shippingAddress, Context $context)
    {
        $isPackstationAddress = $this->packstationService->getPackstationInfo($shippingAddress->jsonSerialize(), $context) !== false;
        $this->logger->debug(__METHOD__, ['isPackstationAddress' => $isPackstationAddress, 'shippingAddress' => $shippingAddress]);

        return $isPackstationAddress;
    }

    private function collectionCanBeSentToPackstation(LineItemCollection $collection): bool
    {
        try {

            foreach ($collection as $lineItem) {
                if (!$this->lineItemCanBeSentToPackstation($lineItem)) {
                    $this->logger->debug(__METHOD__, ['lineItem' => $lineItem->getLabel(), 'canBeSentToPackstation' => false]);
                    return false;
                }
            }

            return true;

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return false;
        }
    }

    private function lineItemCanBeSentToPackstation(LineItem $lineItem): bool
    {
        if ($lineItem->getType() != ProductDefinition::ENTITY_NAME) {
            if ($lineItem->getChildren()->count() > 0) {
                return $this->collectionCanBeSentToPackstation($lineItem->getChildren());
            }

            return true;
        }

       return $this->packstationDeliveryEnabled($lineItem);
    }

    private function packstationDeliveryEnabled(LineItem $lineItem): bool
    {
        $isInvertDeliveryEnabled = $this->pluginConfigUtil->isInvertDeliveryEnabled();
        if ($lineItem->hasPayloadValue('customFields')) {
            $customFields = $lineItem->getPayloadValue('customFields');

            if (count($customFields) === 0 || !isset($customFields[TonurPackstation6::CUSTOMFIELD_PACKSTATION_DELIVERY_ENABLED])) {
                $this->logger->debug(sprintf('%s: customfield "%s" is not set for lineItem (label: %s).', __METHOD__, TonurPackstation6::CUSTOMFIELD_PACKSTATION_DELIVERY_ENABLED, $lineItem->getLabel()), ['lineItem' => $lineItem->getLabel(), 'customFields' => $customFields, 'PluginConfig::invertDeliveryEnabled' => $isInvertDeliveryEnabled]);
                return $isInvertDeliveryEnabled;
            }

            $this->logger->debug(__METHOD__, ['lineItem' => $lineItem->getLabel(), 'customFields' => $customFields, 'PluginConfig::invertDeliveryEnabled' => $isInvertDeliveryEnabled]);
            return $isInvertDeliveryEnabled ? !$customFields[TonurPackstation6::CUSTOMFIELD_PACKSTATION_DELIVERY_ENABLED] : $customFields[TonurPackstation6::CUSTOMFIELD_PACKSTATION_DELIVERY_ENABLED];
        }

        $this->logger->debug(sprintf('%s: lineItem (label: %s) has no customfield.', __METHOD__, $lineItem->getLabel()), ['lineItem' => $lineItem->getLabel(), 'PluginConfig::invertDeliveryEnabled' => $isInvertDeliveryEnabled]);
        return $isInvertDeliveryEnabled;
    }

    /**
     * In Shopware ist ein "Bug" vorhanden, welches Fehlermeldungen mehrmals anzeigt.
     * Auch wird die Fehlermeldung nicht entfernt, wenn sich der Warenkorb aktualisiert (siehe https://issues.shopware.com/issues/NEXT-7281).
     * Nur das Neuladen der Seite bzw. das explizite Löschen der Fehlermeldungen aktualisiert die Fehlermeldungen im Warenkorb.
     *
     * @param Cart $cart
     * @param ErrorCollection $errors
     */
    private function removeErrors(Cart $cart, ErrorCollection $errors)
    {
        $removeErrors = [$this->getProductShippingAddressBlockedError()];

        /** @var Error $removeError */
        foreach ($removeErrors as $removeError) {
            $cart->getErrors()->remove($removeError->getId());
            $errors->remove($removeError->getId());
        }

    }

    private function getProductShippingAddressBlockedError(): ProductShippingAddressBlockedError
    {
        return new ProductShippingAddressBlockedError('address-is-packstation');
    }

    private function isPluginActive(SalesChannelContext $salesChannelContext): bool
    {
        return $this->pluginConfigUtil->isPluginActive($salesChannelContext);
    }

    private function isCartValidatorConfigEnabled(SalesChannelContext $salesChannelContext): bool
    {
        return $this->pluginConfigUtil->isCartValidatorConfigEnabled($salesChannelContext);
    }

}
