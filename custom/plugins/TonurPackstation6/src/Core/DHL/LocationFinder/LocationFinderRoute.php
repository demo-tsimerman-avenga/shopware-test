<?php


namespace Tonur\Packstation\Core\DHL\LocationFinder;

use Exception;
use GuzzleHttp\Client;
use OpenApi\Annotations as OA;
use Psr\Log\LoggerAwareTrait;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\Routing\Annotation\Route;
use Tonur\Packstation\Core\DHL\LocationFinder\Struct\AddressStruct;
use Tonur\Packstation\Util\PluginConfigUtil;

/**
 * @Route(defaults={"_routeScope"={"store-api"}})
 */
class LocationFinderRoute extends AbstractLocationFinderRoute
{
    private CONST SANDBOX_SERVER = "https://api-sandbox.dhl.com/location-finder/v1/";
    private CONST PRODUCTIVE_SERVER = "https://api.dhl.com/location-finder/v1/";

    protected string $endpoint;

    private PluginConfigUtil $pluginConfig;

    private Client $client;

    use LoggerAwareTrait;

    public function __construct(PluginConfigUtil $pluginConfig)
    {
        $this->pluginConfig = $pluginConfig;

        $this->endpoint = $this->pluginConfig->dhlSandbox() ? self::SANDBOX_SERVER : self::PRODUCTIVE_SERVER;

        $config = [
            'base_uri' => $this->endpoint,
            'headers' => [
                'DHL-API-Key' => $this->pluginConfig->dhlApiKey()
            ],
        ];
        $this->client = new Client($config);

    }

    public function getDecorated(): AbstractLocationFinderRoute
    {
        throw new DecorationPatternException(self::class);
    }

    /**
     * @OA\Get(
     *     path="/tonur-packstation/location-finder/find-by-address",
     *     summary="Search for DHL Service Point locations by address",
     * )
     *
     * @Route("/store-api/tonur-packstation/location-finder/find-by-address", name="store-api.tonur-packstation.location-finder.find-by-address", methods={"GET", "POST"})
     */
    public function load(AddressStruct $addressStruct, SalesChannelContext $context): LocationFinderRouteResponse
    {
        try {
            $response = $this->client->request('GET','find-by-address', [
                'query' =>$addressStruct->getVars()
            ]);
            $data = \json_decode($response->getBody()->getContents(), true);

            return new LocationFinderRouteResponse(new ArrayStruct($data));
        } catch (Exception $ex) {

            $this->logger->error(__METHOD__, ['message' => $ex->getMessage(), 'address' => $addressStruct->getVars(), 'exception' => $ex]);
        }

        return new LocationFinderRouteResponse(new ArrayStruct());
    }
}
