<?php


namespace Tonur\Packstation\Core\DHL\LocationFinder;


use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\System\SalesChannel\StoreApiResponse;

class LocationFinderRouteResponse extends StoreApiResponse
{

    /** @var ArrayStruct  */
    protected $object;

    public function __construct(ArrayStruct $object)
    {
        parent::__construct($object);
    }

    public function getLocations(): array
    {
        return $this->object->get('locations') ?? [];
    }

}
