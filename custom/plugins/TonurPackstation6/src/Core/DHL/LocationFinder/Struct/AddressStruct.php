<?php


namespace Tonur\Packstation\Core\DHL\LocationFinder\Struct;


use Shopware\Core\Framework\Struct\Struct;

class AddressStruct extends Struct
{
    protected string $countryCode;

    protected ?string $addressLocality;

    protected ?string $postalCode;

    protected ?string $streetAddress;

    protected ?string $providerType;

    protected ?string $locationType;

    protected ?string $serviceType;

    protected int $radius = 5000;

    protected int $limit = 20;

    protected string $hideClosedLocations = 'false';

    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    public function setCountryCode(string $countryCode): void
    {
        $this->countryCode = $countryCode;
    }

    public function getAddressLocality(): ?string
    {
        return $this->addressLocality;
    }

    public function setAddressLocality(?string $addressLocality): void
    {
        $this->addressLocality = $addressLocality;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    public function getStreetAddress(): ?string
    {
        return $this->streetAddress;
    }

    public function setStreetAddress(?string $streetAddress): void
    {
        $this->streetAddress = $streetAddress;
    }

    public function getProviderType(): ?string
    {
        return $this->providerType;
    }

    public function setProviderType(?string $providerType): void
    {
        $this->providerType = $providerType;
    }

    public function getLocationType(): ?string
    {
        return $this->locationType;
    }

    public function setLocationType(?string $locationType): void
    {
        $this->locationType = $locationType;
    }

    public function getServiceType(): ?string
    {
        return $this->serviceType;
    }

    public function setServiceType(?string $serviceType): void
    {
        $this->serviceType = $serviceType;
    }

    public function getRadius(): int
    {
        return $this->radius;
    }

    public function setRadius(int $radius): void
    {
        $this->radius = $radius;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    public function isHideClosedLocations(): string
    {
        return $this->hideClosedLocations;
    }

    public function setHideClosedLocations(string $hideClosedLocations): void
    {
        $this->hideClosedLocations = $hideClosedLocations;
    }

}
