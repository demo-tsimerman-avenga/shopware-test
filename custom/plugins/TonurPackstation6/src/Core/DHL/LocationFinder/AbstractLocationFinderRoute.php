<?php


namespace Tonur\Packstation\Core\DHL\LocationFinder;


use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Tonur\Packstation\Core\DHL\LocationFinder\Struct\AddressStruct;

abstract class AbstractLocationFinderRoute
{
    abstract public function getDecorated(): AbstractLocationFinderRoute;

    abstract public function load(AddressStruct $addressStruct, SalesChannelContext $context): LocationFinderRouteResponse;

}
