<?php

namespace Tonur\Packstation\Storefront\Framework\Cookie;


use Shopware\Storefront\Framework\Cookie\CookieProviderInterface;
use Tonur\Packstation\Util\PluginConfigUtil;

class CookieProvider implements CookieProviderInterface
{
    public const MAP_COOKIE_NAME = "RepertusPackstationMap";

    private $originalService;

    /** @var PluginConfigUtil */
    private $pluginConfigUtil;

    function __construct(CookieProviderInterface $service,
                        PluginConfigUtil $pluginConfigUtil)
    {
        $this->originalService = $service;
        $this->pluginConfigUtil = $pluginConfigUtil;
    }

    private const PACKSTATION_COOKIES = [
        'snippet_name' => 'cookie.tonur.packstation.group_name',
        'snippet_description' => 'cookie.tonur.packstation.group_description',
        'entries' => [
            [
                'snippet_name' => 'cookie.tonur.packstation.name',
                'cookie' => self::MAP_COOKIE_NAME,
                'value'=> 'cookie value',
            ]
        ],
    ];

    public function getCookieGroups(): array
    {
        $cookieGroups = $this->originalService->getCookieGroups();
        if (!$this->pluginConfigUtil->isPluginActive() || !$this->pluginConfigUtil->isCookieRequestRequired()) {
            return $cookieGroups;
        }

        return array_merge(
            $cookieGroups,
            [
                self::PACKSTATION_COOKIES
            ]
        );
    }
}
