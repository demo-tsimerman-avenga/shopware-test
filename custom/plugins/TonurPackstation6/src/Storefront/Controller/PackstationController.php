<?php declare(strict_types=1);


namespace Tonur\Packstation\Storefront\Controller;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tonur\Packstation\Core\DHL\LocationFinder\AbstractLocationFinderRoute;
use Tonur\Packstation\Core\DHL\LocationFinder\Struct\AddressStruct;
use Tonur\Packstation\Service\PackstationService;
use Tonur\Packstation\Util\PluginConfigUtil;

/**
 * @Route(defaults={"_routeScope"={"storefront"}})
 */
class PackstationController extends StorefrontController
{
    private PackstationService $packstationService;

    private PluginConfigUtil $pluginConfigUtil;

    private TranslatorInterface $translator;

    private AbstractLocationFinderRoute $locationFinderRoute;

    public function __construct(
        AbstractLocationFinderRoute $locationFinderRoute,
        PluginConfigUtil $pluginConfigUtil,
        TranslatorInterface $translator
    )
    {
        $this->locationFinderRoute = $locationFinderRoute;
        $this->pluginConfigUtil = $pluginConfigUtil;
        $this->translator = $translator;
    }

    /**
     * @Route("/tonur/packstation/search", name="frontend.tonur.packstation.search", defaults={"XmlHttpRequest": true}, methods={"POST"})     * @param Request $request
     */
    public function searchPackstation(Request $request, SalesChannelContext $context): JsonResponse
    {
        if (!$this->pluginConfigUtil->isPluginActive($context)) {
            throw new RouteNotFoundException($this->trans('tonur.packstation.messages.route_not_found_exception', ['%saleschannel%' => $context->getSalesChannel()->getTranslation('name')]));
        }

        $street = $request->request->get('packstationStreet');
        $streetNo = $request->request->get('packstationStreetNo');
        $zip = $request->request->get('packstationZip');
        $city = $request->request->get('packstationCity');

        $address = new AddressStruct();
        $address->assign([
            'countryCode' => $request->request->get('countryCode','DE'),
            'addressLocality' => $city,
            'postalCode' => $zip,
            'streetAddress' => $street,
            'limit' => $this->pluginConfigUtil->dhlLimit($context)
        ]);

        $locationFinderResponse = $this->locationFinderRoute->load($address, $context);

        return new JsonResponse($locationFinderResponse->getLocations());
    }

    protected function trans(string $snippet, array $parameters = []): string
    {
        return $this->translator->trans($snippet, $parameters);
    }
}
