export default class TonurPackstationAddScriptTagHelper {

    static add(url) {
        return  new Promise((resolve, reject) => {

            if(document.getElementsByTagName('head')[0].innerHTML.toString().includes(url)) {
                resolve();
                return;
            }

            let tag = this.createScriptTag(url, 'text/javascript');
            tag.async = true;

            document.getElementsByTagName('head')[0].appendChild(tag);

            tag.onload = resolve;
            tag.onerror = reject;

        });
    }

    static createScriptTag(url, type) {
        const tag = document.createElement('script');
        tag.src = url;
        tag.type = type;
        return tag;
    }

}
