import Plugin from 'src/plugin-system/plugin.class';
import NativeEventEmitter from 'src/helper/emitter.helper';
import {TonurPackstationMapModal} from './classes/packstation-map-modal.class';
import {TonurPackstationMapForm} from './classes/packstation-map-form.class';
import UUID from './classes/uuid.class';
import { COOKIE_CONFIGURATION_UPDATE } from 'src/plugin/cookie/cookie-configuration.plugin';
import CookieStorageHelper from 'src/helper/storage/cookie-storage.helper';
import TonurPackstationTestModeHelper from "./helper/packstation-test-mode.helper";

const INLINE_MODE = 0x1;
const MODAL_MODE = 0x2;

/**
 * this plugin creates a map where a packstation can be selected
 */
export default class TonurPackstationMap extends Plugin {

    static options = {
        mapType: null,
        mapApiKey: null,
        snippets: null,
        cookieMissingPermissionSelector: '#TonurPackstationCookieMissingPermission',
        color: {
            packstation: null,
            postoffice: null
        },
        isCookieRequestRequired: true,
        testMode: true,
        validatePostNumber: false
    };

    init() {
        const me = this;

        me.cookieEnabledName = 'RepertusPackstationMap';
        me.$cookieMissingPermissionInfo = $(me.el).next(me.options.cookieMissingPermissionSelector);

        me.$form = $(me.el).closest('form');

        if (!me.$form) {
            throw new Error('Could not find form!');
        }

        me.id = UUID.v4();
        me.$form.attr('data-tonur-packstation-form-id', me.id);
        me.addDataValidation();

        me.handleCookieChangeEvent();

        if (!me.isCookieAccepted()) {
            return;
        }

        me.initPackstationMap();

    }

    _registerEvents() {
        const me = this;

        $(me.el).on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();

            switch (me._mode) {
                case INLINE_MODE:
                    if (me._closeMap()) {
                        return;
                    }

                    me.inlineForm = new TonurPackstationMapForm(
                        me.options.mapType,
                        me.getMapApiKey(),
                        me.options.snippets,
                        me.id,
                        me.options
                    );

                    me.inlineForm.insertBefore(me.$form.find('.repertusPackstationAddressFields'));
                    break;

                case MODAL_MODE:
                default:
                    me.modal = new TonurPackstationMapModal();
                    me.modal.init(
                        me.options.mapType,
                        me.getMapApiKey(),
                        me.options.snippets,
                        me.options
                    );
                    me.modal.show(me.id);
                    break;
            }
        });

        me.emitter.subscribe('TonurPackstation_selectLocation-' + me.id, () => {
            me._closeMap();
        });
    }

    _closeMap() {
        const me = this;

        if (me.modal) {
            me.modal.hide();
            me.modal = null;
            return true;
        } else if (me.inlineForm) {
            me.inlineForm.remove();
            me.inlineForm = null;
            return true;
        }

        return false;
    }

    handleCookieChangeEvent() {
        document.$emitter.subscribe(COOKIE_CONFIGURATION_UPDATE, this.handleCookies.bind(this));
    }

    handleCookies(cookieUpdateEvent) {
        const updatedCookies = cookieUpdateEvent.detail;

        if (!Object.prototype.hasOwnProperty.call(updatedCookies, this.cookieEnabledName)) {
            return;
        }

        this.isCookieAccepted()

        if (updatedCookies[this.cookieEnabledName]) {
            this.initPackstationMap();
            return;
        }

        this.removeEvents()
    }

    initPackstationMap() {
        const me = this;

        if (!me.options.mapType || !me.getMapApiKey()) {
            throw new Error('The options for the packstation map are incomplete!');
        }

        me.emitter = new NativeEventEmitter();

        const $modal = me.$form.closest('.modal');
        me._mode = $modal.length > 0 ? INLINE_MODE : MODAL_MODE;

        let plugins = window.PluginManager.getPluginInstances('TonurPackstationForm');
        if (!!plugins.length) {
            let formPlugin = plugins[0];
            formPlugin.init();
        }

        me._registerEvents();
    }


    removeEvents() {
        $(this.el).unbind();
        this.emitter.unsubscribe('TonurPackstation_selectLocation-' + this.id);
    }


    isCookieAccepted() {
        let me = this,
            accepted = CookieStorageHelper.getItem(me.cookieEnabledName) || !me.options.isCookieRequestRequired;

        if (accepted) {
            me.el.disabled= false;
            if (me.$cookieMissingPermissionInfo) {
                me.$cookieMissingPermissionInfo.addClass('is--hidden');
            }
        } else {
            me.el.disabled= true;
            if (me.$cookieMissingPermissionInfo) {
                me.$cookieMissingPermissionInfo.removeClass('is--hidden');
            }
        }

        return accepted;
    }

    getMapApiKey() {
        return this.options.testMode ? TonurPackstationTestModeHelper.getRepertusBingMapsKey() : this.options.mapApiKey;
    }

    addDataValidation() {
        let me = this;
        if (!me.options.validatePostNumber || me.$form.attr('data-form-validation') !== undefined) {
            return;
        }

        me.$form.attr('data-form-validation','true')
        window.PluginManager.initializePlugins();
    }
}
