export default class TonurPackstationTestModeHelper {

    static getRepertusBingMapsKey() {
        return 'AtPHxHtO2t6M7eIvCEPHxeKS0vxNtTO3bGLEvCsS0UItk1lchazBsky_Ilj7Uvsc';
    }

    static isTestModeKey(key) {
        return key === TonurPackstationTestModeHelper.getRepertusBingMapsKey();
    }

    static handle(key) {
        if (key === this.getRepertusBingMapsKey()) {
            this.showOverlay();
        } else {
            this.hideOverlay();
        }
    }

    static showOverlay() {
        let overlay = $('#packstationTestMode');
        overlay.removeClass('is--hidden');
    }

    static hideOverlay() {
        let overlay = $('#packstationTestMode');
        overlay.addClass('is--hidden');
    }
}
