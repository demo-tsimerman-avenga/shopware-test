import BaseMap from './base-map.class';
import NativeEventEmitter from 'src/helper/emitter.helper';
import TonurPackstationAddScriptTagHelper from "../helper/packstation-add-script-tag.helper";
import TonurPackstationTestModeHelper from "../helper/packstation-test-mode.helper";

export default class BingMap extends BaseMap {

    mapContainer;
    mapApiKey;
    snippets;
    emitter;
    id;
    url = 'https://www.bing.com/maps/sdkrelease/mapcontrol';
    config;

    constructor(mapContainer, mapApiKey, snippets, id, config) {
        super();
        this.mapContainer = mapContainer;
        this.mapApiKey = mapApiKey;
        this.snippets = snippets;
        this.id = id;
        this.emitter = new NativeEventEmitter();
        this.config = config;
    }

    loadMap() {
        const me = this;

        TonurPackstationAddScriptTagHelper.add(
            me.url
        ).then( () => {

            if (!me.initMap()) {
                let intervalNumber = setInterval(function () {
                    console.log('trying initialing map...');

                    if(me.initMap()) {
                        console.log('...map initialized successfully.');
                        clearInterval(intervalNumber);
                    }

                }, 700);
            }

        }).catch( (error) => {
            console.log('An error occurred while loading script: ' + me.url);
            console.log(error);
            throw error;
        } );
    }

    initMap() {
        const me = this;
        try {
            me.map = new window.Microsoft.Maps.Map(me.mapContainer,
                {
                    credentials: me.mapApiKey,
                    enableSearchLogo: false,
                    enableClickableLogo: false,
                    showDashboard: false,
                    showMapTypeSelector: false,
                    showScalebar: false,
                    zoom: 13
                }
            );

            me.addPushPinLegend();
            TonurPackstationTestModeHelper.handle(me.mapApiKey);

            return true;

        } catch (exception) {
            console.log('An error occurred initialing Bing Map.');
            console.log(exception);
            return false
        }
    }

    addLocations(locations) {
        const me = this;
        let first = true;
        const infoBox = new window.Microsoft.Maps.Infobox(new window.Microsoft.Maps.Location(0, 0),
            {
                visible: false,
                offset: new window.Microsoft.Maps.Point(10, 20),
                height: 110,
                width: 250,
                showCloseButton: true,
                zIndex: 99,
                actions: [
                    {
                        label: me.snippets.select_packstation,
                        eventHandler: function () {
                            me._selectLocation(me.target);
                        }
                    }
                ]
            });

        me.map.entities.clear();

        let pinConfig = {};
        $(locations).each(function () {
            pinConfig = me.setColorConfig(this, pinConfig);

            const pin = new window.Microsoft.Maps.Pushpin(
                new window.Microsoft.Maps.Location(this.location.latitude, this.location.longitude),
                pinConfig
            );

            window.Microsoft.Maps.Events.addHandler(pin, 'click', function () {
                me.map.setView({center: pin.getLocation()});
            });

            if (first) {
                first = false;
                me.map.setView({center: new window.Microsoft.Maps.Location(this.location.latitude, this.location.longitude)});
            }

            pin.title = this.title;
            pin.description = this.description;
            pin.data_number = this.data_number;
            pin.data_address = this.data_address;
            pin.data_type = this.data_type;

            me.map.entities.push(pin);

            const handler = function (e) {
                if (e.targetType === 'pushpin') {
                    const location = e.target.getLocation();
                    me.target = e.target;

                    infoBox.setLocation(location);
                    infoBox.setOptions(
                        {
                            visible: true,
                            title: me.target.title,
                            description: me.target.description
                        }
                    );
                }
            };

            window.Microsoft.Maps.Events.addHandler(pin, 'click', handler);
            window.Microsoft.Maps.Events.addHandler(pin, 'mouseover', handler);

        });

        infoBox.setMap(me.map);
    }

    prepareLocations(locations) {
        const locationData = [];

        $(locations).each(function () {
            if (this.location.keyword === null || this.location.keyword === "" || this.location.keywordId === null || this.location.keywordId === "") {
                return;
            }

            let type = null;
            switch (this.location.type) {
                case 'postoffice':
                case 'postbank':
                case 'servicepoint':
                    type = 'F';
                    break;
                case 'locker':
                    type = 'P'
                    break;
            }

            if (type=== null) {
                return;
            }

            let title = `${this.location.keyword} ${this.location.keywordId}`.trim();

            let description = `${this.place.address.streetAddress}<br/>${this.place.address.postalCode} ${this.place.address.addressLocality}`.trim();
            if (title !== this.name) {
                description = `${this.name}<br/>${description}`.trim();
            }

            locationData.push({
                'location': this.place.geo,
                'title': title,
                'description': description,
                'data_type': type,
                'data_number': this.location.keywordId,
                'data_address': this.place.address
            })

        });

        return locationData;
    }

    _selectLocation(target) {
        const me = this;

        me.emitter.publish('TonurPackstation_selectLocation-' + me.id, {
            'address': target.data_address,
            'type': target.data_type,
            'number': target.data_number
        });
    }

    setColorConfig(location, config) {

        if (this.config === undefined || !this.config || this.config.color === undefined || !this.config.color) {
            delete config.color;
            return config;
        }

        let pinColor = null;
        if (this.isPackstation(location) && this.config.color.packstation) {
            pinColor = this.config.color.packstation;
        } else if(this.isPostoffice(location) && this.config.color.postoffice) {
            pinColor = this.config.color.postoffice
        }

        config.color = pinColor;
        return config;
    }

    addPushPinLegend() {

        if (this.config === undefined || this.config.shopPushPinLegend === null || !this.config.shopPushPinLegend) {
            return;
        }

        let items = ''
        if (this.config.color.packstation) {
            items += '<li><span style="background-color:' + this.config.color.packstation +';"></span>Packstation</li>';
        }

        if (this.config.color.postoffice) {
            items += '<li><span style="background-color:' + this.config.color.postoffice +';"></span>Postfiliale</li>';
        }

        document.getElementById('packstationLegend').innerHTML =
            '<ul class="legend-labels">' + items + '</ul>';
    }

}
