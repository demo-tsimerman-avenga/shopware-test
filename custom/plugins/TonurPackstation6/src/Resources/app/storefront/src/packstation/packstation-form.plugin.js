import Plugin from 'src/plugin-system/plugin.class';
import NativeEventEmitter from 'src/helper/emitter.helper';
import { addressTypes, packstationTypes } from './constants/packstation-constants';
import DomAccess from 'src/helper/dom-access.helper';

/**
 * this plugin handles the form for the packstation
 */
export default class TonurPackstationForm extends Plugin {

    static options = {
        streetValues: {
            packstation: 'Packstation',
            postOffice: 'Postfiliale'
        },
        selectors: {
            formSelector: 'form',
            addressFormSubmit: '.address-form-submit',
            addressType: 'input[type="radio"][name="repertusPackstationAddressType"]',
            addressTypePackstation: '#repertusPackstationAddressType1',
            packstationFieldSet: '.repertusPackstationAddressFields',
            packstationType: '#repertusPackstationAddressPackstationType',
            packstationNumber: '#repertusPackstationAddressPackstationNumber',
            dhlNumber: '#repertusPackstationAddressPostNumber',
            dhlNumberSaveTo: '#shippingAddressAdditionalField1',
            city: '#shippingAddressAddressCity',
            zip: '#shippingAddressAddressZipcode',
            street: '#shippingAddressAddressStreet',
            country: '#shippingAddressAddressCountry',
            countryState: '#shippingAddressAddressCountryState',
            department: '#addressdepartment',
            vatId: '#addressvatId',
            cookieButton: '#repertusPackstationOpenCookie'
        },
        config: {
            savePostnumberField: 'additionalAddressLine1'
        },
        countryIdGermany: null
    };

    init() {
        const me = this;

        me._initDefaultValues();

        me.emitter = new NativeEventEmitter();

        me.formToggleCheckbox = DomAccess.querySelector(document, '#differentShippingAddress', false);
        if (me.formToggleCheckbox) {
            me.formFieldTogglePluginEmitter = new NativeEventEmitter(me.formToggleCheckbox);
        }

        me.$el = $(me.el);
        me.$form = me.$el.closest(me.options.selectors.formSelector);
        me.id = me.$form.attr('data-tonur-packstation-form-id');
        me.$addressTypeSelection = me.$el.find(me.options.selectors.addressType);
        me.$packstationFieldSet = me.$el.find(me.options.selectors.packstationFieldSet);
        me.$packstationNumberField = me.$el.find(me.options.selectors.packstationNumber);
        me.$dhlNumberField = me.$el.find(me.options.selectors.dhlNumber);
        me.$packstationTypeField = me.$el.find(me.options.selectors.packstationType);
        me.$zipField = me.$form.find(me.options.selectors.zip);
        me.$cityField = me.$form.find(me.options.selectors.city);
        me.$countryField = me.$form.find(me.options.selectors.country);
        me.$countryStateField = me.$form.find(me.options.selectors.countryState);
        me.$street = me.$form.find(me.options.selectors.street);
        me.$dhlNumberSaveToField = me.$form.find(me.options.selectors.dhlNumberSaveTo);
        me.$departmentField = me.$form.find(me.options.selectors.department);
        me.$vatIdField = me.$form.find(me.options.selectors.vatId);
        me.$cookieButton = me.$el.find(me.options.selectors.cookieButton);

        me._registerEvents();

        this._handleRelevantDataChanged(false);
    }

    _initDefaultValues() {
        this.currentAddressType = addressTypes.MAIL;
        this.currentPackstationType = packstationTypes.UNKNOWN;
        this.currentDhlNumber = '';
        this.currentPackstationNumber = '';
    }

    _registerEvents() {
        this.emitter.subscribe('TonurPackstation_selectLocation-' + this.id, (event) => {
            this._setPackstationDataToForm(event.detail);
            this._handleRelevantDataChanged();
        });

        if (this.formToggleCheckbox) {
            this.formFieldTogglePluginEmitter.subscribe('onChange', () => {
                this._updateView();
            });
        }

        this.$addressTypeSelection.on('change', $.proxy(this._onRelevantDataForStreetChanged, this));
        this.$packstationNumberField.on('change', $.proxy(this._onRelevantDataForStreetChanged, this));
        this.$packstationTypeField.on('change', $.proxy(this._onRelevantDataForStreetChanged, this));
        this.$dhlNumberField.on('change', $.proxy(this._onRelevantDataForStreetChanged, this));

        this.$cookieButton.on('click', (event) => {
            event.preventDefault();
            window.PluginManager.getPluginInstances('CookieConfiguration').forEach((instance) => {
                instance.openOffCanvas();
            });
        });
    }

    _onRelevantDataForStreetChanged () {
        this._handleRelevantDataChanged();
    }

    _handleRelevantDataChanged(updateFormFields = true) {
        this._updateCurrentValues();
        this._calculateStreetValue();

        if (updateFormFields) {
            this._updateFormFields();
            this._setCountryToGermanyIfNecessary();
        }

        this._updateView();
    }

    _updateCurrentValues() {
        this._updateCurrentAddressType();
        this._updateCurrentPackstationType();
        this._updateDhlNumber();
        this._updatePackstationNumber();
        this._updateCurrentZip();
        this._updateCurrentCity();
    }

    _updateCurrentAddressType() {
        this.currentAddressType = this.$addressTypeSelection.filter(':checked').val();
    }

    _updateCurrentPackstationType() {
        this.currentPackstationType = this.$packstationTypeField.val();
    }

    _updateDhlNumber() {
        this.currentDhlNumber = this.$dhlNumberField.val();
    }

    _updatePackstationNumber() {
        this.currentPackstationNumber = this.$packstationNumberField.val();
    }

    _updateCurrentZip() {
        this.currentZip = this.$zipField.val();
    }

    _updateCurrentCity() {
        this.currentCity = this.$cityField.val();
    }

    _updateFormFields() {
        this.$street.val(this.currentStreet);
        this.$dhlNumberSaveToField.val(this.currentDhlNumber);
        this.$packstationNumberField.val(this.currentPackstationNumber);
        this.$dhlNumberField.val(this.currentDhlNumber);
        this.$zipField.val(this.currentZip);
        this.$cityField.val(this.currentCity);

        this._setCountryToInitialValue();
    }

    _updateView() {
        const isPackstationOrPostOffice =
            this.currentAddressType === addressTypes.PACKSTATION ||
            this.currentAddressType === addressTypes.POST_OFFICE;
        const saveInCompanyField = this.options.config.savePostnumberField === 'company';

        this.$packstationFieldSet.toggleClass('d-none', !isPackstationOrPostOffice);
        this.$packstationFieldSet.find('input').prop('disabled', !isPackstationOrPostOffice);
        this.$street.parent().toggle(!isPackstationOrPostOffice);
        this.$dhlNumberSaveToField.parent().toggle(!isPackstationOrPostOffice);

        this.$departmentField.parent().toggleClass('d-none', isPackstationOrPostOffice && saveInCompanyField);
        this.$vatIdField.parent().toggleClass('d-none', isPackstationOrPostOffice && saveInCompanyField);

        this.$emitter.publish('TonurPackstationForm/updateView', {
            'id': this.id,
            'isPackstationOrPostOffice': isPackstationOrPostOffice
        });
    }

    _setCountryToGermanyIfNecessary() {
        if (this.currentAddressType === addressTypes.PACKSTATION ||
            this.currentAddressType === addressTypes.POST_OFFICE) {

            if (this.options.countryIdGermany !== null &&
                this.$countryField.val() !== this.options.countryIdGermany) {
                this.$countryField.val(this.options.countryIdGermany);

                this._fireChangeEventForDOMElement(this.$countryField[0]);
            }
        }
    }

    _setCountryToInitialValue() {
        if (this.currentAddressType === addressTypes.MAIL) {
            try {
                const initialCountryId = this.$countryField.data('initialCountryId');

                if (initialCountryId === this.options.countryIdGermany) {
                    return;
                }

                this.$countryField.val(initialCountryId);
                if (initialCountryId) {
                    this._fireChangeEventForDOMElement(this.$countryField[0]);
                } else {
                    // hide country state
                    this.$countryStateField.parent().toggleClass('d-none', true);
                    this.$countryStateField.attr('disabled', 'disabled');
                }
            } catch (exception) {
                console.log('An error occurred setting country to initial value');
                console.log(exception);
            }
        }
    }

    _fireChangeEventForDOMElement(DOMElement, shouldBubble = false, isCancelable = true) {
        if (DOMElement === undefined) {
            return ;
        }

        const evt = document.createEvent('HTMLEvents');
        evt.initEvent('change', shouldBubble, isCancelable);

        DOMElement.dispatchEvent(evt);
    }

    _setPackstationDataToForm(data) {
        this.$packstationNumberField.val(data.number);
        this.$packstationTypeField.val(data.type);
        this.$zipField.val(data.address.postalCode);
        this.$cityField.val(data.address.addressLocality);

        this.$addressTypeSelection.filter('[value=' + addressTypes.PACKSTATION + ']').trigger('click');
    }

    _calculateStreetValue() {
        const me = this;

        if (me.currentPackstationNumber === '' || me.currentAddressType === addressTypes.MAIL) {
            me.street =  '';
            me.currentStreet = '';
            me.currentZip = '';
            me.currentCity = '';
            if (me.currentAddressType === addressTypes.MAIL) {
                me.currentPackstationNumber = '';
                me.currentDhlNumber = '';
            }
            return;
        }

        me.currentStreet = me.options.streetValues.packstation;

        if (me.currentPackstationType !== packstationTypes.PACKSTATION) {
            me.currentStreet =  me.options.streetValues.postOffice;
        }

        me.currentStreet += ' ' + me.currentPackstationNumber;
    }
}
