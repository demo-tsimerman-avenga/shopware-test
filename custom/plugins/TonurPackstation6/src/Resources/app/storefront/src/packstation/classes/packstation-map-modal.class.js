import {TonurPackstationMapForm} from './packstation-map-form.class';

export class TonurPackstationMapModal {

    constructor() {
        if (!TonurPackstationMapModal.instance) {
            this._data = {
                $element: null
            };
            this.options = {
                mapType: null,
                mapApiKey: null,
                snippets: null,
                config: null
            };

            TonurPackstationMapModal.instance = this;
        }

        return TonurPackstationMapModal.instance;
    }

    init(mapType, mapApiKey, snippets, config) {
        const me = this;

        me.options.mapType = mapType;
        me.options.mapApiKey = mapApiKey;
        me.options.snippets = snippets;
        me.options.config = config;
    }

    _getModalTemplate() {
        const me = this;

        return '<div id="repertusPackstationMapModal" tabindex="-1" class="repertusPackstationMapModal fade" aria-hidden="true">\n' +
            '   <div class="modal-lg modal-dialog modal-dialog-centered" role="document">\n' +
            '       <div class="modal-content">\n' +
            '           <div class="modal-header">\n' +
            '               <h5 class="modal-title" id="repertusPackstationMapModalLabel">' + me.options.snippets.search_packstation_post_office + '</h5>\n' +
            '               <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
            '                   <span aria-hidden="true">&times;</span>\n' +
            '               </button>\n' +
            '           </div>\n' +
            '           <div id="repertusPackstationMapModalBody" class="modal-body"></div>\n' +
            '       </div>\n' +
            '   </div>\n' +
            '</div>'
    }

    show(id) {
        const me = this;

        if (me._data.$element) {
            return false;
        }

        const $element = $(me._getModalTemplate()).insertAfter('body > div.js-pseudo-modal-template');
        me._data.$element = $element;

        $element.modal('show');

        $element.on('shown.bs.modal', function () {
            const form = new TonurPackstationMapForm(me.options.mapType, me.options.mapApiKey, me.options.snippets, id, me.options.config);

            form.append($element.find('#repertusPackstationMapModalBody'));
        });

        $element.on('hidden.bs.modal', function () {
            $element.remove();
            me._data.$element = null;
        });

        return true;
    }

    hide() {
        const me = this;
        const $element = me._data.$element;

        if (!$element) {
            return false;
        }

        $element.modal('hide');

        return true;
    }
}

const instance = new TonurPackstationMapModal();
Object.freeze(instance);

export default instance;
