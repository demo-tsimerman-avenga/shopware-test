import Plugin from 'src/plugin-system/plugin.class';


export default class TonurPackstationAcrisSeparateStreetCSPlugin extends Plugin {
    static options = {
        selectors: {
            houseNumber: '#shippingAddressAddressHouseNumber',
            addressTypePackstation: '#repertusPackstationAddressType1',
        }
    }

    init() {
        const me = this;
        me.$el = $(me.el);

        const acrisSeparateStreetOptions = JSON.parse(me.$el.attr('data-acris-separate-street-options'));

        let houseNumberSelector = me.options.selectors.houseNumber;
        if (acrisSeparateStreetOptions && acrisSeparateStreetOptions.inputId) {
            houseNumberSelector = acrisSeparateStreetOptions.inputId;
        }

        if (houseNumberSelector.indexOf('billingAddress') !== -1) {
            return;
        }

        if (houseNumberSelector.indexOf('AddressHouseNumber') === -1 ) {
            return;
        }

        me.$form = me.$el.closest('form');
        me.id = me.$form.attr('data-tonur-packstation-form-id');

        if (me.id === undefined) { // Rechnungsadresse oder keine Packstation vorhanden
            return;
        }

        me.$addressTypeSelection = me.$el.find(me.options.selectors.addressType);
        me.$houseNumber = me.$form.find(houseNumberSelector);

        me._registerEvents();

        me.triggerUpdatePackstationView();
    }

    _registerEvents() {
        const pluginRegistry = window.PluginManager;

        pluginRegistry.getPluginInstances('TonurPackstationForm').forEach((pluginInstance) => {
            pluginInstance.$emitter.subscribe('TonurPackstationForm/updateView', this._updateView.bind(this));
        });
    }

    _updateView(event) {
        if (this.id !== event.detail.id) {
            return;
        }

        const isPackstationOrPostOffice = event.detail.isPackstationOrPostOffice;

        this.$houseNumber.attr('disabled', isPackstationOrPostOffice);
        if (isPackstationOrPostOffice) {
            this.$houseNumber.val('');
        }
        this.$houseNumber.parent().toggle(!isPackstationOrPostOffice);

    }

    triggerUpdatePackstationView() {
        const me = this,
            pluginRegistry = window.PluginManager;

        pluginRegistry.getPluginInstances('TonurPackstationForm').forEach((pluginInstance) => {

            if (pluginInstance.id !== me.id) {
                return;
            }

            pluginInstance._handleRelevantDataChanged(false);
        });
    }

}
