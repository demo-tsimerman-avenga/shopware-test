const TYPE_PACKSTATION = 'P';
const TYPE_POSTOFFICE = 'F';

export default class BaseMap {
    constructor() {
        if (this.loadMap === undefined) {
            throw new TypeError('Must override method loadMap');
        }
        if (this.addLocations === undefined) {
            throw new TypeError('Must override method addLocations');
        }
        if (this.prepareLocations === undefined) {
            throw new TypeError('Must override method prepareLocations');
        }
    }

    isPackstation(location) {
        return location.data_type === TYPE_PACKSTATION;
    }

    isPostoffice(location) {
        return location.data_type === TYPE_POSTOFFICE;
    }
}
