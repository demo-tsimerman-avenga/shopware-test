// Import all necessary Storefront plugins and scss files
import TonurPackstationMap from './packstation/packstation-map.plugin';
import TonurPackstationForm from './packstation/packstation-form.plugin';
import TonurPackstationAcrisSeparateStreetCSPlugin from "./packstation/third-party-plugins/packstation.acris.separate-street-cs.plugin";

// Register them via the existing PluginManager
const PluginManager = window.PluginManager;
PluginManager.register('TonurPackstationMap', TonurPackstationMap, '[data-tonur-packstation-map]');
PluginManager.register('TonurPackstationForm', TonurPackstationForm, '[data-tonur-packstation-form]');
PluginManager.register('TonurPackstationAcrisSeparateStreetCSPlugin', TonurPackstationAcrisSeparateStreetCSPlugin, '[data-acris-separate-street]');

// Necessary for the webpack hot module reloading server
if (module.hot) {
    module.hot.accept();
}
