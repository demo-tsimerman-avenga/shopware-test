import BingMap from '../maps/bing-map.class';
import ElementLoadingIndicatorUtil from 'src/utility/loading-indicator/element-loading-indicator.util';
import HttpClient from 'src/service/http-client.service';
import UUID from './uuid.class';

export class TonurPackstationMapForm {

    constructor(mapType, mapApiKey, snippets, id, config) {
        const me = this;

        me._client = new HttpClient(window.accessKey, window.contextToken);

        me.options = {
            mapType: mapType,
            mapApiKey: mapApiKey,
            snippets: snippets,
            config: config
        };

        me.id = id ? id : UUID.v4();
    }

    _getBodyTemplate() {
        const me = this;

        return  '<div class="packstation">\n' +
                '<div class="alert alert-warning" id="packstationAlert-' + me.id + '" role="alert" hidden>\n' +
                    '<div class="alert-content-container">' +
                        '<div class="alert-content">' +
                            me.options.snippets.no_packstation_found +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '   <div class="form-row">\n' +
                '       <div class="form-group col-4">\n' +
                '           <label class="form-label" for="packstationZip-' + me.id + '">' + me.options.snippets.zip + '*</label>\n' +
                '           <input form="tonur-packstation-search-form" class="form-control" name="packstationZip" id="packstationZip-' + me.id + '" type="text" placeholder="' + me.options.snippets.zip + '" required="required"/>\n' +
                '       </div>\n' +
                '       <div class="form-group col-8">\n' +
                '           <label class="form-label" for="packstationCity-' + me.id + '">' + me.options.snippets.city + '*</label>\n' +
                '           <input form="tonur-packstation-search-form" class="form-control" name="packstationCity" id="packstationCity-' + me.id + '" type="text" placeholder="' + me.options.snippets.city + '" required="required"/>\n' +
                '       </div>\n' +
                '       <div class="form-group col-12">\n' +
                '           <button form="tonur-packstation-search-form" class="btn btn-primary btn-block" type="button" id="searchPackstation-' + me.id + '">' + me.options.snippets.search + '</button>\n' +
                '       </div>\n' +
                '   </div>\n' +
                '   <div id="packstationLegend" class="legend"></div>\n'+
                '   <div id="packstationTestMode" class="tonur-packstation-map-test-mode-overlay is--hidden">Test mode</div>\n'+
                '   <div id="packstationMap-' + me.id + '"></div>\n' +
                '</div>'
        ;
    }

    _loadPackstationMap(selector) {
        const me = this;

        switch (me.options.mapType) {
            case 'bingMaps':
            default:
                me.map = new BingMap(selector, me.options.mapApiKey, me.options.snippets, me.id, me.options.config);
                break;
        }

        me.map.loadMap();
    }

    _searchPackstation(form) {
        const me = this;
        const searchUrl = form.attr('action');
        const serializedData = form.serializeArray();
        const formData = {};

        if (!me.$element || me.searching) {
            return;
        }

        const alert = me.$element.find('#packstationAlert-' + me.id);
        alert.attr("hidden", true);

        me.searching = true;
        ElementLoadingIndicatorUtil.create(me.$element[0]);

        $.each(serializedData, function() {
            formData[this.name] = this.value;
        });

        me._doSearch(searchUrl, formData);
    }

    _doSearch(formUrl, data) {
        const me = this;
        this._client.post(formUrl, JSON.stringify(data), (response) => {
            const responseData = JSON.parse(response);

            if (responseData.length === 0) {
                const alert = me.$element.find('#packstationAlert-' + me.id);
                alert.removeAttr("hidden");
            }

            me.map.addLocations(me.map.prepareLocations(responseData));
            ElementLoadingIndicatorUtil.remove(me.$element[0]);
            me.searching = false;
        });
    }

    _registerEvents() {
        const me = this;

        const button = me.$element.find('#searchPackstation-' + me.id);

        button.on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();

            const $searchForm = $(event.target.form);

            const $zipInput = me.$element.find('#packstationZip-' + me.id);
            const $cityInput = me.$element.find('#packstationCity-' + me.id);
            const zip = $zipInput.val();
            const city = $cityInput.val();

            $zipInput.toggleClass('is-invalid', !zip.trim());
            $cityInput.toggleClass('is-invalid', !city.trim());

            if (!zip.trim() || !city.trim()) {
                return;
            }

            me._searchPackstation($searchForm);
        });

        me.$element.find('input').on('keyup keypress', function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                button.trigger('click');
            }
        });
    }

    insertBefore($element) {
        const me = this;

        if (me.$element === null) {
            return null;
        }

        const result = $(me._getBodyTemplate()).insertBefore($element);

        me.$element = result;

        me._loadPackstationMap('#packstationMap-' + me.id);
        me._registerEvents();

        return result;
    }

    insertAfter($element) {
        const me = this;

        if (me.$element === null) {
            return null;
        }

        const result = $(me._getBodyTemplate()).insertAfter($element);

        me.$element = result;

        this._loadPackstationMap('#packstationMap-' + me.id);
        me._registerEvents();

        return result;
    }

    append($element) {
        const me = this;

        if (me.$element === null) {
            return null;
        }

        const result = $($element).append(me._getBodyTemplate());

        me.$element = result;

        this._loadPackstationMap('#packstationMap-' + me.id);
        me._registerEvents();

        return result;
    }

    remove() {
        const me = this;

        if (me.$element === null) {
            return;
        }

        me.$element.remove();
        me.$element = null;
    }
}
