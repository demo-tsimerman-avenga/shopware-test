export const addressTypes = {
    MAIL: '0',
    PACKSTATION: '1',
    POST_OFFICE: '2'
};

export const packstationTypes = {
    UNKNOWN: '',
    PACKSTATION: 'P',
    POST_OFFICE: 'F'
};
