import template from './tonur-condition-customer-shipping-packstation.html.twig';

const { Component } = Shopware;
const { mapPropertyErrors } = Component.getComponentHelper();

/**
 * @public
 * @description Condition for the ShippingPackstationRule. This component must a be child of sw-condition-tree.
 * @status prototype
 * @example-type code-only
 * @component-example
 * <tonur-condition-customer-shipping-packstation :condition="condition" :level="0"></tonur-condition-customer-shipping-packstation>
 */
Component.extend('tonur-condition-customer-shipping-packstation', 'sw-condition-base', {
    template,

    computed: {
        selectValues() {
            return [
                {
                    label: this.$tc('global.sw-condition.condition.yes'),
                    value: true
                },
                {
                    label: this.$tc('global.sw-condition.condition.no'),
                    value: false
                }
            ];
        },

        isCustomerShippingPackstation: {
            get() {
                this.ensureValueExist();
                return this.condition.value.isCustomerShippingPackstation;
            },
            set(isCustomerShippingPackstation) {
                this.ensureValueExist();
                this.condition.value = { ...this.condition.value, isCustomerShippingPackstation };
            }
        },

        ...mapPropertyErrors('condition', ['value.isCustomerShippingPackstation']),

        currentError() {
            return this.conditionValueIsCustomerShippingPackstationError;
        }
    },
});
