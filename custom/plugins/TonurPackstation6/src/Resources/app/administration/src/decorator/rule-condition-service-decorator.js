import '../app/component/rule/condition-type/tonur-condition-customer-shipping-packstation';

Shopware.Application.addServiceProviderDecorator('ruleConditionDataProviderService', (ruleConditionService) => {
    ruleConditionService.addCondition('customerShippingPackstation', {
        component: 'tonur-condition-customer-shipping-packstation',
        label: 'tonur.packstation.shipping_rule',
        scopes: ['checkout']
    });

    return ruleConditionService;
});
