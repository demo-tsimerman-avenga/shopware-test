import './decorator/rule-condition-service-decorator';
import deDE from './snippet/de-DE';
import enGB from './snippet/en-GB';

Shopware.Locale.extend('de-DE', deDE);
Shopware.Locale.extend('en-GB', enGB);
