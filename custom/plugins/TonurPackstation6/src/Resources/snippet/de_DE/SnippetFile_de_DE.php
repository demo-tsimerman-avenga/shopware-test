<?php declare(strict_types=1);

namespace Tonur\Packstation\Resources\snippet\de_DE;

use Shopware\Core\System\Snippet\Files\SnippetFileInterface;

class SnippetFile_de_DE implements SnippetFileInterface
{
    public function getName(): string
    {
        return 'packstation.de-DE';
    }

    public function getPath(): string
    {
        return __DIR__ . '/packstation.de-DE.json';
    }

    public function getIso(): string
    {
        return 'de-DE';
    }

    public function getAuthor(): string
    {
        return 'Repertus GmbH';
    }

    public function isBase(): bool
    {
        return false;
    }
}
