<?php declare(strict_types=1);

namespace Tonur\Packstation\Controller;

use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tonur\Packstation\Service\PackstationService;
use Tonur\Packstation\TonurPackstation6;

/**
 * @RouteScope(scopes={"sales-channel-api"})
 */
class PackstationController extends AbstractController
{
    /**
     * @var PackstationService
     */
    private $packstationService;

    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    /** @var TranslatorInterface */
    private $translator;

    /**
     * @param PackstationService $packstationService
     * @param SystemConfigService $systemConfigService
     * @param TranslatorInterface $translator
     */
    public function __construct(
        PackstationService $packstationService,
        SystemConfigService $systemConfigService,
        TranslatorInterface $translator
    )
    {
        $this->packstationService = $packstationService;
        $this->systemConfigService = $systemConfigService;
        $this->translator = $translator;
    }

    /**
     * @Route("/sales-channel-api/v1/tonur/packstation/search", name="sales-channel-api.action.tonur.packstation.search", methods={"POST"})
     * @param Request $request
     * @param SalesChannelContext $context
     * @return JsonResponse
     */
    public function searchPackstation(Request $request, SalesChannelContext $context)
    {
        if (!$this->systemConfigService->get(TonurPackstation6::BUNDLE_NAME . '.config.pluginActive', $context->getSalesChannel()->getId())) {
            throw new RouteNotFoundException($this->trans('tonur.packstation.messages.route_not_found_exception', ['%saleschannel%' => $context->getSalesChannel()->getTranslation('name')]));
        }

        $street = $request->request->get('street');
        $streetNo = $request->request->get('streetNo');
        $zip = $request->request->get('zip');
        $city = $request->request->get('city');

        $packstation = $this->packstationService->searchPackstation($street, $streetNo, $zip, $city);

        return new JsonResponse($packstation);
    }

    protected function trans(string $snippet, array $parameters = []): string
    {
        return $this->translator->trans($snippet, $parameters);
    }
}
