<?php

namespace Tonur\Packstation\Util;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Processor\PsrLogMessageProcessor;
use Psr\Log\LoggerInterface;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class LoggerFactory
{
    private $rotatingFilePathPattern = '';

    private int $defaultFileRotationCount;

    public function __construct(string $rotatingFilePathPattern, int $defaultFileRotationCount = 14)
    {
        $this->rotatingFilePathPattern = $rotatingFilePathPattern;
        $this->defaultFileRotationCount = $defaultFileRotationCount;
    }

    public function createRotating(string $filePrefix, SystemConfigService $systemConfigService, ?int $fileRotationCount = null, int $loggerLevel = Logger::DEBUG): LoggerInterface
    {
        $filepath = sprintf($this->rotatingFilePathPattern, $filePrefix);

        $result = new Logger($filePrefix, $systemConfigService);
        $result->pushHandler((new RotatingFileHandler($filepath, $fileRotationCount ?? $this->defaultFileRotationCount, $loggerLevel)));
        $result->pushProcessor((new PsrLogMessageProcessor()));

        return $result;
    }


}
