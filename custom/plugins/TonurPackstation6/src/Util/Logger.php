<?php

namespace Tonur\Packstation\Util;

use DateTimeZone;
use Monolog\DateTimeImmutable;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Tonur\Packstation\TonurPackstation6;
use Monolog\Logger as MonologLogger;

class Logger extends MonologLogger
{
    /** @var bool  */
    private $debugMode;

    public function __construct($name, SystemConfigService $systemConfigService, array $handlers = [], array $processors = [], ?DateTimeZone $timezone = null)
    {
        $this->debugMode = $systemConfigService->getBool(TonurPackstation6::BUNDLE_NAME . '.config.debugMode');

        parent::__construct($name, $handlers, $processors, $timezone);
    }

    public function addRecord($level, $message, array $context = [], DateTimeImmutable $datetime = null) :bool
    {
        if (!$this->debugMode) {
            return false;
        }

        $addRecord = new \ReflectionMethod(MonologLogger::class, 'addRecord');

        if ($addRecord->getNumberOfParameters() === 3) {
            return parent::addRecord($level, $message, $context);
        }

        return parent::addRecord($level, $message, $context, $datetime);
    }



}
