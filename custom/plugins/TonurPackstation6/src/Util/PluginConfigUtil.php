<?php

namespace Tonur\Packstation\Util;


use Exception;
use Psr\Log\LoggerAwareTrait;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Tonur\Packstation\TonurPackstation6;

class PluginConfigUtil
{
    /** @var SystemConfigService */
    private $systemConfigService;

    use LoggerAwareTrait;

    public function __construct(SystemConfigService $systemConfigService)
    {
        $this->systemConfigService = $systemConfigService;
    }

    public function isPluginActive(?SalesChannelContext $salesChannelContext = null): bool
    {
        $pluginActiveConfig =  $this->systemConfigService->getBool(
            TonurPackstation6::BUNDLE_NAME . '.config.pluginActive',
            $salesChannelContext ? $salesChannelContext->getSalesChannel()->getId() : null
        );

        $this->log(__METHOD__, ['pluginActiveConfig' => $pluginActiveConfig], $salesChannelContext);

        return $pluginActiveConfig;
    }

    public function isCartValidatorConfigEnabled(?SalesChannelContext $salesChannelContext = null): bool
    {
        $cartValidatorConfig =  $this->systemConfigService->getBool(
            TonurPackstation6::BUNDLE_NAME . '.config.cartValidator',
            $salesChannelContext ? $salesChannelContext->getSalesChannel()->getId() : null);

        $this->log(__METHOD__, ['cartValidatorConfig' => $cartValidatorConfig], $salesChannelContext);

        return $cartValidatorConfig;
    }

    public function colorPackstation(?SalesChannelContext $salesChannelContext = null)
    {
        $colorPackstationConfig = $this->systemConfigService->get(
            TonurPackstation6::BUNDLE_NAME . '.config.colorPackstation',
            $salesChannelContext ? $salesChannelContext->getSalesChannel()->getId() : null);

        $this->log(__METHOD__, ['colorPackstationConfig' => $colorPackstationConfig], $salesChannelContext);

        return $colorPackstationConfig;
    }

    public function colorPostoffice(?SalesChannelContext $salesChannelContext = null)
    {
        $colorPostofficeConfig = $this->systemConfigService->get(
            TonurPackstation6::BUNDLE_NAME . '.config.colorPostoffice',
            $salesChannelContext ? $salesChannelContext->getSalesChannel()->getId() : null);

        $this->log(__METHOD__, ['colorPostofficeConfig' => $colorPostofficeConfig], $salesChannelContext);

        return $colorPostofficeConfig;
    }

    public function isCookieRequestRequired(?SalesChannelContext $salesChannelContext = null): bool
    {
        $cookieRequestRequiredConfig = $this->systemConfigService->getBool(
            TonurPackstation6::BUNDLE_NAME . '.config.cookieRequestRequired',
            $salesChannelContext ? $salesChannelContext->getSalesChannel()->getId() : null);

        $this->log(__METHOD__, ['cookieRequestRequiredConfig' => $cookieRequestRequiredConfig], $salesChannelContext);

        return $cookieRequestRequiredConfig;
    }

    public function isInvertDeliveryEnabled(?SalesChannelContext $salesChannelContext = null): bool
    {
        $isInvertDeliveryEnabled = $this->systemConfigService->getBool(
            TonurPackstation6::BUNDLE_NAME . '.config.invertDeliveryEnabled',
            $salesChannelContext ? $salesChannelContext->getSalesChannel()->getId() : null);

        $this->log(__METHOD__, ['cookieRequestRequiredConfig' => $isInvertDeliveryEnabled], $salesChannelContext);

        return $isInvertDeliveryEnabled;
    }

    public function validatePostNumber(?SalesChannelContext $salesChannelContext = null): bool
    {
        $validatePostNumberConfig = $this->systemConfigService->getBool(
            TonurPackstation6::BUNDLE_NAME . '.config.validatePostNumber',
            $salesChannelContext ? $salesChannelContext->getSalesChannel()->getId() : null);

        $this->log(__METHOD__, ['validatePostNumberConfig' => $validatePostNumberConfig], $salesChannelContext);

        return $validatePostNumberConfig;
    }

    public function dhlSandbox(?SalesChannelContext $salesChannelContext = null): bool
    {
        $dhlSandbox = $this->systemConfigService->getBool(
            TonurPackstation6::BUNDLE_NAME . '.config.dhlSandbox',
            $salesChannelContext ? $salesChannelContext->getSalesChannel()->getId() : null
        );

        $this->log(__METHOD__, ['dhlSandboxConfig' => $dhlSandbox], $salesChannelContext);

        return $dhlSandbox;
    }

    public function dhlApiKey(?SalesChannelContext $salesChannelContext = null): string
    {
        $dhlApiKey = $this->systemConfigService->getString(
            TonurPackstation6::BUNDLE_NAME . '.config.dhlApiKey',
            $salesChannelContext ? $salesChannelContext->getSalesChannel()->getId() : null
        );

        if (empty($dhlApiKey)) {
            $dhlApiKey = 'H6GeVSOAfYQxo2rLVAYtinA4e3Ty7Vnd';
        }

        $this->log(__METHOD__, ['dhlApiKeyConfig' => $dhlApiKey], $salesChannelContext);

        return $dhlApiKey;
    }

    public function dhlLimit(?SalesChannelContext $salesChannelContext = null)
    {
        $dhlLimit = $this->systemConfigService->getInt(
            TonurPackstation6::BUNDLE_NAME . '.config.dhlLimit',
            $salesChannelContext ? $salesChannelContext->getSalesChannel()->getId() : null
        );

        $this->log(__METHOD__, ['dhlLimitConfig' => $dhlLimit], $salesChannelContext);

        return $dhlLimit;
    }

    private function log($message, array $context = array(), ?SalesChannelContext $salesChannelContext = null)
    {
        try {
            $context['SalesChannel'] = $salesChannelContext ? $salesChannelContext->getSalesChannel()->getTranslation('name') : null;
            $this->logger->debug($message, $context);
        } catch (Exception $e) {
            $this->logger->error($message, ['exception' => $e]);
        }
    }

}
