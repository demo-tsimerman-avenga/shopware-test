<?php declare(strict_types=1);

namespace Tonur\Packstation;

use Doctrine\DBAL\Connection;
use Exception;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\ContainsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Shopware\Core\Framework\Plugin\Context\UpdateContext;
use Shopware\Core\System\CustomField\CustomFieldTypes;

class TonurPackstation6 extends Plugin
{
    public const BUNDLE_NAME = 'TonurPackstation6';
    public const CUSTOMFIELD_SET_PACKSTATION_PRODUCT = "repertus_packstation_product";
    public const CUSTOMFIELD_PACKSTATION_DELIVERY_ENABLED = "repertus_packstation_delivery_enabled";

    /** @var EntityRepositoryInterface $customFieldSetRepository */
    private $customFieldSetRepository;

    public function install(InstallContext $installContext): void
    {
        parent::install($installContext);
        $this->addCustomFieldSet($installContext->getContext());
    }

    /**
     * @param UninstallContext $uninstallContext
     */
    public function uninstall(UninstallContext $uninstallContext): void
    {
        parent::uninstall($uninstallContext);

        if ($uninstallContext->keepUserData()) {
            return;
        }

        $this->removeCustomFieldSet($uninstallContext->getContext());
        $this->removeConfiguration($uninstallContext->getContext());
        $this->removeDatabaseData();

    }

    public function update(UpdateContext $updateContext): void
    {
        parent::update($updateContext);

        $this->addCustomFieldSet($updateContext->getContext());
    }

    private function removeDatabaseData()
    {
        $connection = $this->container->get(Connection::class);
        $connection->executeUpdate('DROP TABLE IF EXISTS `repertus_packstation`');
    }

    private function removeCustomFieldSet(Context $context)
    {
        try {

            $criteria = new Criteria();
            $criteria->addFilter(new EqualsFilter('name', self::CUSTOMFIELD_SET_PACKSTATION_PRODUCT));

            $idSearchResult = $this->getCustomFieldSetRepository()->searchIds($criteria, $context);

            $ids = \array_map(static function ($id) {
                return ['id' => $id];
            }, $idSearchResult->getIds());

            $this->getCustomFieldSetRepository()->delete($ids, $context);


        } catch (Exception $e) {
            error_log(sprintf('%s: %s', __METHOD__, $e->getMessage()));
            throw $e;
        }
    }

    private function removeConfiguration(Context $context): void
    {
        /** @var EntityRepositoryInterface $systemConfigRepository */
        $systemConfigRepository = $this->container->get('system_config.repository');

        $criteria = (new Criteria())
            ->addFilter(new ContainsFilter('configurationKey', self::BUNDLE_NAME . '.config'));
        $idSearchResult = $systemConfigRepository->searchIds($criteria, $context);

        $ids = \array_map(static function ($id) {
            return ['id' => $id];
        }, $idSearchResult->getIds());

        $systemConfigRepository->delete($ids, $context);
    }


    private function addCustomFieldSet(Context $context)
    {
        try {
            $criteria = new Criteria();

            $criteria
                ->addAssociation('customFields')
                ->addFilter(new EqualsFilter('customFields.name', self::CUSTOMFIELD_PACKSTATION_DELIVERY_ENABLED));

            $searchResult = $this->getCustomFieldSetRepository()->search($criteria, $context);

            if ($searchResult->count() == 0) {
                $this->getCustomFieldSetRepository()->create([
                    [
                        'name' => self::CUSTOMFIELD_SET_PACKSTATION_PRODUCT,
                        'config' => [
                            'label' => [
                                'de-DE' => 'Packstation',
                                'en-GB' => 'Packstation'
                            ]
                        ],
                        'relations' => [[
                            'entityName' => 'product'
                        ]],
                        'customFields' => [
                            [
                                'name' => self::CUSTOMFIELD_PACKSTATION_DELIVERY_ENABLED,
                                'type' => CustomFieldTypes::BOOL,
                                'config' => [
                                    'type' => CustomFieldTypes::SWITCH,
                                    'label' => [
                                        "de-DE" => "Lieferung an Packstation möglich",
                                        "en-GB" => "Delivery to packstation is possible"
                                    ],
                                    "componentName" => "sw-field",
                                    "customFieldType" => CustomFieldTypes::SWITCH,
                                    "customFieldPosition" => 1
                                ]
                            ]
                        ]
                    ]
                ], $context);

            }
        } catch (Exception $e) {
            error_log(sprintf('%s: %s', __METHOD__, $e->getMessage()));
            throw $e;
        }

    }

    private function getCustomFieldSetRepository(): EntityRepositoryInterface
    {
        if ($this->customFieldSetRepository === null) {
            $this->customFieldSetRepository = $this->container->get('custom_field_set.repository');
        }

        return $this->customFieldSetRepository;
    }
}
