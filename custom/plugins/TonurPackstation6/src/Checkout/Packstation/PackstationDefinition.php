<?php declare(strict_types=1);

namespace Tonur\Packstation\Checkout\Packstation;

use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CreatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\UpdatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class PackstationDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'repertus_packstation';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return PackstationEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new FkField('customer_address_id', 'customerAddressId' , CustomerAddressDefinition::class))->addFlags(new Required()),
            (new StringField('post_number', 'postNumber'))->addFlags(new Required()),
            (new StringField('packstation_type', 'packstationType'))->addFlags(new Required()),
            (new StringField('packstation_number', 'packstationNumber'))->addFlags(new Required()),
            new CreatedAtField(),
            new UpdatedAtField(),
            new OneToOneAssociationField('customerAddress', 'customer_address_id', 'id', CustomerAddressDefinition::class, false),
        ]);
    }
}
