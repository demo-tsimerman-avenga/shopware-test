<?php declare(strict_types=1);

namespace Tonur\Packstation\Checkout\Packstation;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class PackstationEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var string
     */
    protected $postNumber;

    /**
     * @var string
     */
    protected $packstationType;

    /**
     * @var string
     */
    protected $packstationNumber;

    /**
     * @return string
     */
    public function getPostNumber(): string
    {
        return $this->postNumber;
    }

    /**
     * @param string $postNumber
     */
    public function setPostNumber(string $postNumber): void
    {
        $this->postNumber = $postNumber;
    }

    /**
     * @return string
     */
    public function getPackstationType(): string
    {
        return $this->packstationType;
    }

    /**
     * @param string $packstationType
     */
    public function setPackstationType(string $packstationType): void
    {
        $this->packstationType = $packstationType;
    }

    /**
     * @return string
     */
    public function getPackstationNumber(): string
    {
        return $this->packstationNumber;
    }

    /**
     * @param string $packstationNumber
     */
    public function setPackstationNumber(string $packstationNumber): void
    {
        $this->packstationNumber = $packstationNumber;
    }
}
