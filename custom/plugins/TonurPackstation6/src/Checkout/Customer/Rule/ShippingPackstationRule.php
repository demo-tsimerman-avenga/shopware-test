<?php declare(strict_types=1);

namespace Tonur\Packstation\Checkout\Customer\Rule;

use Shopware\Core\Checkout\CheckoutRuleScope;
use Shopware\Core\Framework\Rule\Rule;
use Shopware\Core\Framework\Rule\RuleScope;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;

class ShippingPackstationRule extends Rule
{
    /**
     * @var bool
     */
    protected $isCustomerShippingPackstation;

    public function __construct(bool $isCustomerShippingPackstation = true)
    {
        parent::__construct();
        $this->isCustomerShippingPackstation = $isCustomerShippingPackstation;
    }

    public function match(RuleScope $scope): bool
    {
        if (!$scope instanceof CheckoutRuleScope) {
            return false;
        }

        /** @var CheckoutRuleScope $scope */
        if (!$location = $scope->getSalesChannelContext()->getShippingLocation()->getAddress()) {
            return false === $this->isCustomerShippingPackstation;
        }

        return ($location->getExtension('repertusPackstation') !== null) === $this->isCustomerShippingPackstation;
    }

    public function getConstraints(): array
    {
        return [
            'isCustomerShippingPackstation' => [new NotNull(), new Type('bool')],
        ];
    }

    public function getName(): string
    {
        return 'customerShippingPackstation';
    }
}
