<?php declare(strict_types=1);

namespace Tonur\Packstation\Checkout\Extension;

use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Tonur\Packstation\Checkout\Packstation\PackstationDefinition;

class CustomerAddressExtension extends EntityExtension
{
    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(
            new OneToOneAssociationField('repertusPackstation', 'id', 'customer_address_id', PackstationDefinition::class)
        );
    }

    public function getDefinitionClass(): string
    {
        return CustomerAddressDefinition::class;
    }
}
