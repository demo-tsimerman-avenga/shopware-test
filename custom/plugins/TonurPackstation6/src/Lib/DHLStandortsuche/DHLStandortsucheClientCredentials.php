<?php
namespace Tonur\Packstation\Lib\DHLStandortsuche;

/**
 * Plugin "PackstationPlus"
 *
 * Copyright 2014 Tonur UG
 * All rights reserved.
 */

/**
 * Class DHLStandortSucheClientCredentials.
 */
class DHLStandortsucheClientCredentials {

    /**
     *
     * @var cig_user $cig_user
     * @access public
     */
    public $cig_user;

    /**
     *
     * @var cig_password $cig_password
     * @access cig_password
     */
    public $cig_password;

    /**
     *
     * @var cig_environment $cig_environment
     * @access cig_environment
     */
    public $cig_environment;

    /**
     *
     * @var cig_environment $cig_environment
     * @access cig_environment
     */
    public $cig_endpoint;

    /**
     * Constructs credentials instance.
     *
     * @param cig_user $cig_user
     * @param cig_password $cig_password
     * @param cig_environment $cig_environment
     * @param cig_endpoint $cig_endpoint
     * @access public
     */
    public function __construct($cig_user, $cig_password, $cig_environment, $cig_endpoint)
    {
        $this->cig_user = $cig_user;
        $this->cig_password = $cig_password;
        $this->cig_environment = $cig_environment;
        $this->cig_endpoint = $cig_endpoint;
    }
}
