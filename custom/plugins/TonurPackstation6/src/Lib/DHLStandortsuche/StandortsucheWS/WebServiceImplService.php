<?php
/**
 * GENERATED CODE - DO NOT EDIT!!!
 */

include_once('getPackstationsByAddress.php');
include_once('inputAddress.php');
include_once('getPackstationsByAddressResponse.php');
include_once('automatWS.php');
include_once('timeinfos.php');
include_once('address.php');
include_once('location.php');
include_once('branch.php');
include_once('serviceTypes.php');
include_once('automats.php');
include_once('poboxes.php');
include_once('servicesAddition.php');
include_once('pobox.php');
include_once('timeinfo.php');
include_once('ServiceException.php');
include_once('getPackstationsFilialeDirektByAddress.php');
include_once('getPackstationsFilialeDirektByAddressResponse.php');
include_once('automatFD.php');
include_once('getPaketboxesByAddress.php');
include_once('getPaketboxesByAddressResponse.php');
include_once('getBranchesByAddress.php');
include_once('inputTimeinfo.php');
include_once('getBranchesByAddressResponse.php');
include_once('getPackstationsPaketboxesByAddress.php');
include_once('getPackstationsPaketboxesByAddressResponse.php');


/**
 *  Generated WebServiceImplService class.
 */
class WebServiceImplService extends \SoapClient
{

  /**
   * 
   * @var array $classmap The defined classes
   * @access private
   */
  private static $classmap = array(
    'getPackstationsByAddress' => '\\getPackstationsByAddress',
    'inputAddress' => '\\inputAddress',
    'getPackstationsByAddressResponse' => '\\getPackstationsByAddressResponse',
    'automatWS' => '\\automatWS',
    'timeinfos' => '\\timeinfos',
    'address' => '\\address',
    'location' => '\\location',
    'branch' => '\\branch',
    'serviceTypes' => '\\serviceTypes',
    'automats' => '\\automats',
    'poboxes' => '\\poboxes',
    'timeinfos' => '\\timeinfos',
    'servicesAddition' => '\\servicesAddition',
    'pobox' => '\\pobox',
    'timeinfos' => '\\timeinfos',
    'timeinfo' => '\\timeinfo',
    'ServiceException' => '\\ServiceException',
    'getPackstationsFilialeDirektByAddress' => '\\getPackstationsFilialeDirektByAddress',
    'getPackstationsFilialeDirektByAddressResponse' => '\\getPackstationsFilialeDirektByAddressResponse',
    'automatFD' => '\\automatFD',
    'timeinfos' => '\\timeinfos',
    'serviceTypes' => '\\serviceTypes',
    'poboxes' => '\\poboxes',
    'servicesAddition' => '\\servicesAddition',
    'getPaketboxesByAddress' => '\\getPaketboxesByAddress',
    'getPaketboxesByAddressResponse' => '\\getPaketboxesByAddressResponse',
    'getBranchesByAddress' => '\\getBranchesByAddress',
    'inputTimeinfo' => '\\inputTimeinfo',
    'getBranchesByAddressResponse' => '\\getBranchesByAddressResponse',
    'getPackstationsPaketboxesByAddress' => '\\getPackstationsPaketboxesByAddress',
    'getPackstationsPaketboxesByAddressResponse' => '\\getPackstationsPaketboxesByAddressResponse');

  /**
   * Generated constructor.
   * @param array $options A array of config values
   * @param string $wsdl The wsdl file to use
   * @access public
   */
  public function __construct(array $options = array(), $wsdl = 'https://cig.dhl.de/cig-wsdls/com/dpdhl/wsdl/standortsuche-api/1.0/standortsuche-api-1.0.wsdl')
  {
    foreach (self::$classmap as $key => $value) {
    if (!isset($options['classmap'][$key])) {
      $options['classmap'][$key] = $value;
    }
  }
  
  parent::__construct($wsdl, $options);
  }

  /**
   * Generated method.
   * @param getPackstationsByAddress $parameters
   * @access public
   * @return getPackstationsByAddressResponse
   */
  public function getPackstationsByAddress(getPackstationsByAddress $parameters)
  {
    return $this->__soapCall('getPackstationsByAddress', array($parameters));
  }

  /**
   * Generated method.
   * @param getPackstationsPaketboxesByAddress $parameters
   * @access public
   * @return getPackstationsPaketboxesByAddressResponse
   */
  public function getPackstationsPaketboxesByAddress(getPackstationsPaketboxesByAddress $parameters)
  {
    return $this->__soapCall('getPackstationsPaketboxesByAddress', array($parameters));
  }

  /**
   * Generated method.
   * @param getPaketboxesByAddress $parameters
   * @access public
   * @return getPaketboxesByAddressResponse
   */
  public function getPaketboxesByAddress(getPaketboxesByAddress $parameters)
  {
    return $this->__soapCall('getPaketboxesByAddress', array($parameters));
  }

  /**
   * Generated method.
   * @param getPackstationsFilialeDirektByAddress $parameters
   * @access public
   * @return getPackstationsFilialeDirektByAddressResponse
   */
  public function getPackstationsFilialeDirektByAddress(getPackstationsFilialeDirektByAddress $parameters)
  {
    return $this->__soapCall('getPackstationsFilialeDirektByAddress', array($parameters));
  }

  /**
   * Generated method.
   * @param getBranchesByAddress $parameters
   * @access public
   * @return getBranchesByAddressResponse
   */
  public function getBranchesByAddress(getBranchesByAddress $parameters)
  {
    return $this->__soapCall('getBranchesByAddress', array($parameters));
  }
}
