<?php
/**
 * GENERATED CODE - DO NOT EDIT!!!
 */

class timeinfos
{

  /**
   * 
   * @var timeinfo $timeinfo
   * @access public
   */
  public $timeinfo = null;

  /**
   * Generated constructor.
   * @param timeinfo $timeinfo
   * @access public
   */
  public function __construct($timeinfo)
  {
    $this->timeinfo = $timeinfo;
  }

}
