<?php
/**
 * GENERATED CODE - DO NOT EDIT!!!
 */

class getPackstationsByAddressResponse
{

  /**
   * 
   * @var automatWS $packstation
   * @access public
   */
  public $packstation = null;

  /**
   * Generated constructor.
   * @param automatWS $packstation
   * @access public
   */
  public function __construct($packstation)
  {
    $this->packstation = $packstation;
  }

}
