<?php
/**
 * GENERATED CODE - DO NOT EDIT!!!
 */

class getBranchesByAddressResponse
{

  /**
   * 
   * @var branch $branch
   * @access public
   */
  public $branch = null;

  /**
   * Generated constructor.
   * @param branch $branch
   * @access public
   */
  public function __construct($branch)
  {
    $this->branch = $branch;
  }

}
