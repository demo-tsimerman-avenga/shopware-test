<?php
/**
 * GENERATED CODE - DO NOT EDIT!!!
 */

class poboxes
{

  /**
   * 
   * @var pobox $pobox
   * @access public
   */
  public $pobox = null;

  /**
   * Generated constructor.
   * @param pobox $pobox
   * @access public
   */
  public function __construct($pobox)
  {
    $this->pobox = $pobox;
  }

}
