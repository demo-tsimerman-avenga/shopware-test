<?php
/**
 * GENERATED CODE - DO NOT EDIT!!!
 */

class serviceTypes
{

  /**
   * 
   * @var int $serviceType
   * @access public
   */
  public $serviceType = null;

  /**
   * Generated constructor.
   * @param int $serviceType
   * @access public
   */
  public function __construct($serviceType)
  {
    $this->serviceType = $serviceType;
  }

}
