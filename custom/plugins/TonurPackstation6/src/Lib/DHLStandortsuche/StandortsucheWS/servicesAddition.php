<?php
/**
 * GENERATED CODE - DO NOT EDIT!!!
 */

class servicesAddition
{

  /**
   * 
   * @var string $serviceAddition
   * @access public
   */
  public $serviceAddition = null;

  /**
   * Generated constructor.
   * @param string $serviceAddition
   * @access public
   */
  public function __construct($serviceAddition)
  {
    $this->serviceAddition = $serviceAddition;
  }

}
