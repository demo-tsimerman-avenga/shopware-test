<?php
/**
 * GENERATED CODE - DO NOT EDIT!!!
 */

class automats
{

  /**
   * 
   * @var automatWS $automat
   * @access public
   */
  public $automat = null;

  /**
   * Generated constructor.
   * @param automatWS $automat
   * @access public
   */
  public function __construct($automat)
  {
    $this->automat = $automat;
  }

}
