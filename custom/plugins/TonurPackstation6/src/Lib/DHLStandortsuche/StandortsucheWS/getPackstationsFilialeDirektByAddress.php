<?php
/**
 * GENERATED CODE - DO NOT EDIT!!!
 */

class getPackstationsFilialeDirektByAddress
{

  /**
   * 
   * @var string $key
   * @access public
   */
  public $key = null;

  /**
   * 
   * @var inputAddress $address
   * @access public
   */
  public $address = null;

  /**
   * Generated constructor.
   * @param string $key
   * @param inputAddress $address
   * @access public
   */
  public function __construct($key, $address)
  {
    $this->key = $key;
    $this->address = $address;
  }

}
