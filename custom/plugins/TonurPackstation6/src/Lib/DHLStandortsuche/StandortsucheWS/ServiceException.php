<?php
/**
 * GENERATED CODE - DO NOT EDIT!!!
 */

/**
 * Generated class ServiceException.
 */
class ServiceException
{

  /**
   * 
   * @var string $id
   * @access public
   */
  public $id = null;

  /**
   * 
   * @var string $message
   * @access public
   */
  public $message = null;

  /**
   * Generated constructor.
   * @param string $id
   * @param string $message
   * @access public
   */
  public function __construct($id, $message)
  {
    $this->id = $id;
    $this->message = $message;
  }

}
