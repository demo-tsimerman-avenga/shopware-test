<?php
/**
 * GENERATED CODE - DO NOT EDIT!!!
 */

class getPaketboxesByAddressResponse
{

  /**
   * 
   * @var automatWS $paketbox
   * @access public
   */
  public $paketbox = null;

  /**
   * Generated constructor.
   * @param automatWS $paketbox
   * @access public
   */
  public function __construct($paketbox)
  {
    $this->paketbox = $paketbox;
  }

}
