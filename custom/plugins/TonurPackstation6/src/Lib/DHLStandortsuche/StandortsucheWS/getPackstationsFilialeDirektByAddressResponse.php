<?php
/**
 * GENERATED CODE - DO NOT EDIT!!!
 */

class getPackstationsFilialeDirektByAddressResponse
{

  /**
   * 
   * @var automatFD $packstation_filialedirekt
   * @access public
   */
  public $packstation_filialedirekt = null;

  /**
   * Generated constructor.
   * @param automatFD $packstation_filialedirekt
   * @access public
   */
  public function __construct($packstation_filialedirekt)
  {
    $this->packstation_filialedirekt = $packstation_filialedirekt;
  }

}
