<?php
namespace Tonur\Packstation\Lib\DHLStandortsuche;

/**
 * Plugin "PackstationPlus"
 *
 * Copyright 2014 Tonur UG
 * All rights reserved.
 */

const standortsucheWSDir = "StandortsucheWS/";

const PRODURL = 'https://cig.dhl.de/services/production/soap';
const SANDBOXURL = 'https://cig.dhl.de/services/sandbox/soap';

const SETTINGS = "settings/settings.ini";
const KEY_KEY = "key";
const KEY_USER = "user";
const KEY_PASSWORD = "pwd_key";
const KEY_APPLICATION = "application";
const KEY_TOKEN = "token";
const KEY_ENVIRONMENT = "environment";

const PRODUCTION = 'PRODUCTION';
const SANDBOX = 'SANDBOX';

/**
 * Class DHLStandortsucheClient
 */
class DHLStandortsucheClient
{
    private $requestBuilder = null;
    private $settings_array = null;
    private $credentials = null;
    private $KEY = null;
    private $standortSucheService = null;

    /**
     * Constructs a DHLStandortsucheClient instance.
     */
    function __construct()
    {
        $this->requestBuilder = new DHLStandortsucheRequestBuilder();
        $this->settings_array = parse_ini_file(SETTINGS);
        $this->KEY = $this->settings_array[KEY_KEY];

        if (strcasecmp($this->settings_array[KEY_ENVIRONMENT], PRODUCTION) == 0) {
            $this->credentials =
                new DHLStandortsucheClientCredentials($this->settings_array[KEY_APPLICATION],
                    $this->settings_array[KEY_TOKEN],
                    $this->settings_array[KEY_ENVIRONMENT],
                    PRODURL);
        } else {
            $this->credentials =
                new DHLStandortsucheClientCredentials($this->settings_array[KEY_USER],
                    $this->settings_array[KEY_PASSWORD],
                    $this->settings_array[KEY_ENVIRONMENT],
                    SANDBOXURL);
        }

        $context = stream_context_create([
            'ssl' => [
                // set some SSL/TLS specific options
                'verify_peer'       => false,
                'verify_peer_name'  => false,
                'allow_self_signed' => true
                ]
            ]);

        $this->standortSucheService = new \SoapClient(
            "https://cig.dhl.de/cig-wsdls/com/dpdhl/wsdl/standortsuche-api/1.1/standortsuche-api-1.1.wsdl",

            array('login' => $this->credentials->cig_user,
                'password' => $this->credentials->cig_password,
                'location' => $this->credentials->cig_endpoint,
                'trace' => 1,
                'exceptions' => 0,
                'stream_context' => $context
            )
        );
    }

    /**
     * Determines all Packstations by address.
     * @param $street
     * @param $streetNo
     * @param $zip
     * @param $city
     * @return mixed
     */
    public function determinePackstationsByAddress($street, $streetNo, $zip, $city)
    {
        $address = $this->requestBuilder->createNewInputAddress($street, $streetNo, $zip, $city);
        $getPackstationsByAddr = $this->requestBuilder->getPackstationsByAddr($this->KEY, $address);
        $getPackstationsByAddrResponse = $this->standortSucheService->__soapCall('getPackstationsByAddress', array($getPackstationsByAddr));
        $packstations = $getPackstationsByAddrResponse->packstation;

        return $packstations;
    }

    /**
     * Determines all branches by address.
     * @param $street
     * @param $streetNo
     * @param $zip
     * @param $city
     * @return mixed
     */
    public function determineBranchesByAddress($street, $streetNo, $zip, $city)
    {
        $address = $this->requestBuilder->createNewInputAddress($street, $streetNo, $zip, $city);
        $getBranchesByAddr = $this->requestBuilder->getBranchesByAddr($this->KEY, $address);
        $getBranchesByAddrResponse = $this->standortSucheService->__soapCall('getBranchesByAddress', array($getBranchesByAddr));
        $branches = $getBranchesByAddrResponse->branch;

        return $branches;
    }

    /**
     * Determines all Packstations and Filialen by address.
     * @param $street
     * @param $streetNo
     * @param $zip
     * @param $city
     * @return mixed
     */
    public function determinePackstationsFilialeDirektByAddress($street, $streetNo, $zip, $city)
    {
        $address = $this->requestBuilder->createNewInputAddress($street, $streetNo, $zip, $city);
        $getPackstationsFilialeDirektByAddr = $this->requestBuilder->getPackstationsFilialeDirektByAddr($this->KEY, $address);
        $getPackstationsFilialeDirektByAddrResponse = $this->standortSucheService->__soapCall('getPackstationsFilialeDirektByAddress', array($getPackstationsFilialeDirektByAddr));
        $packstations = $getPackstationsFilialeDirektByAddrResponse->packstation_filialedirekt;

        return $packstations;
    }

}
