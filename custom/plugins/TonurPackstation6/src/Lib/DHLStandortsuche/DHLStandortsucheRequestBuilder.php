<?php
namespace Tonur\Packstation\Lib\DHLStandortsuche;

/**
 * Plugin "PackstationPlus"
 *
 * Copyright 2014 Tonur UG
 * All rights reserved.
 */

require_once 'StandortsucheWS/WebServiceImplService.php';

/**
 * Class Client_DHLStandortsuche_DHLStandortsucheRequestBuilder
 */
class DHLStandortsucheRequestBuilder
{
    public $encoding = "UTF8";
    public $defaultBranchService = "0";

    /**
     * Erstellt inputAddress mit angegebenen Werten und gibt diese zurück.
     *
     * @param $street
     * @param $streetNo
     * @param $zip
     * @param $city
     * @return inputAddress
     */
    public function createNewInputAddress($street, $streetNo, $zip, $city)
    {
        return new \inputAddress($street, $streetNo, $zip, $city);
    }

    /**
     * Erstellt inputTimeInfo mit angegebenen Werten und gibt diese zurück.
     *
     * @param $is_open_today
     * @param $weekday
     * @param $time
     * @return mixed
     */
    public function createTimeInfo($is_open_today, $weekday, $time)
    {
        $isOpenedToday = filter_var($is_open_today, FILTER_VALIDATE_BOOLEAN);
        return new \inputTimeinfo($isOpenedToday, $weekday, $time);
    }

    /**
     * getPackstationsFilialeDirektByAddr
     * @param $key
     * @param $inputAddr
     * @return getPackstationsFilialeDirektByAddress
     */
    public function getPackstationsFilialeDirektByAddr($key, $inputAddr)
    {
        return new \getPackstationsFilialeDirektByAddress($key, $inputAddr);
    }

    /**
     * getPackstationsPaketboxesByAddr
     * @param $key
     * @param $inputAddr
     * @return getPackstationsPaketboxesByAddress
     */
    public function getPackstationsPaketboxesByAddr($key, $inputAddr)
    {
        return new \getPackstationsPaketboxesByAddress($key, $inputAddr);
    }

    /**
     * getPaketboxesByAddr
     * @param $key
     * @param $inputAddr
     * @return getPaketboxesByAddress
     */
    public function getPaketboxesByAddr($key, $inputAddr)
    {
        return new \getPaketboxesByAddress($key, $inputAddr);
    }

    /**
     * getBranchesByAddr
     * @param $key
     * @param $inputAddr
     * @return GetBranchesByAddress
     */
    public function getBranchesByAddr($key, $inputAddr)
    {
        return new \GetBranchesByAddress(
            $key,
            $inputAddr,
            $this->defaultBranchService,
            $this->createDefaultTimeInfo(),
            true,
            false);
    }

    /**
     * createDefaultTimeInfo
     * @return inputTimeinfo
     */
    public function createDefaultTimeInfo()
    {
        $isOpenedToday = "0";
        $weekday = "";
        $time = "";
        return new \inputTimeinfo($isOpenedToday, $weekday, $time);
    }

    /**
     * getPackstationsByAddr
     * @param $key
     * @param $inputAddr
     * @return getPackstationsByAddress
     */
    public function getPackstationsByAddr($key, $inputAddr)
    {
        return new \getPackstationsByAddress($key, $inputAddr);
    }
}
