# 1.0.2
- Behebt ein mögliches Problem, bei dem der Cookie Consent Manager im Checkout nicht bedient werden konnte.
- Behebt ein mögliches Problem, der beim Aufrufen der Adressen auftreten kann.

# 1.0.1
- Kompatibilität zu Shopware 6.3.4.1 hergestellt.

# 1.0.0
- Neues Feature: Cookie Einstellung hinzugefügt. Bevor die Packstationssuche verwendet werden kann, muss der Cookie akzeptiert werden.
- Verbessert die Fehlermeldungsanzeige im Warenkorb
- Plugineinstellung zum Debuggen
- Plugineinstellung zur Validierung des Warenkorbs hinzugefügt. 
  Wenn eine Packstation/Postfiliale als Lieferadresse gewählt ist und im Warenkorb ein Produkt vorhanden ist, dass nicht an eine Packstation/Postfiliale gesendet werden können, ist das Abschließen der Bestellung nicht möglich.

# 0.6.0
- Neues Feature: Am Artikel in den Zusatzfeldern kann man nun einstellen, ob ein Artikel an eine Packstation versendet werden kann.

# 0.5.9
- Renewal of the Bing Map Credentials

# 0.5.8
- Behebt ein mögliches Problem bei Angabe einer abweichender Lieferadresse
- Behebt ein mögliches Routing Problem 

# 0.5.7
- Das Plugin verwendet nun die HTTPS Bing Maps API

# 0.5.6
- Stellt die Kompatibilität mit Shopware 6.1.3 her

# 0.5.5
- Erste Version unseres Plugin Packstation für Shopware 6
