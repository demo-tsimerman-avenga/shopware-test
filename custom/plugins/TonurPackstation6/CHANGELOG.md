#1.0.2
- Resolves a possible problem in which the Cookie Consent Manager could not be used in the checkout.
- Resolves a possible problem that can occur while requesting the addresses.

# 1.0.1
- Compatibility with Shopware 6.3.4.1 established.

# 1.0.0
- New feature: Added cookie setting. Before the Packstation search can be used, the cookie must be accepted.
- Improves the error message display in the shopping cart.
- Added plugin setting for debugging.
- Added plugin setting for cart validating.

# 0.6.0
- New Feature: In the article settings in the additional fields you can now set whether an article can be sent to a packstation.

# 0.5.9
- Bing Map Api Key erneuert

# 0.5.8
- Resolves a possible problem when specifying a different delivery address
- Resolves a possible routing problem

# 0.5.7
- The plugin now uses the HTTPS Bing Maps API

# 0.5.6
- Provides compatibility with Shopware 6.1.3

# 0.5.5
- First version of our Plugin Packstation for Shopware 6
